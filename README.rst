============
ID19 project
============

[![build status](https://gitlab.esrf.fr/ID19/id19/badges/master/build.svg)](http://ID19.gitlab-pages.esrf.fr/ID19)
[![coverage report](https://gitlab.esrf.fr/ID19/id19/badges/master/coverage.svg)](http://ID19.gitlab-pages.esrf.fr/id19/htmlcov)

ID19 software & configuration

Latest documentation from master can be found [here](http://ID19.gitlab-pages.esrf.fr/id19)
