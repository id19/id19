from bliss.setup_globals import *
from bliss.common.scans import ct, sct
from bliss.common.logtools import log_info, log_debug, log_warning

class ImageCorrection():
    def __init__(self, detector, shutter, expo_time=1.0):
        self.detector = detector
        self.shutter  = shutter
        self.exposure_time = expo_time
        
        self.__dark_image = None
        self.__flat_image = None
    
    
    def reset(self):
        self.detector.processing.background = ''
        self.detector.processing.flatfield = ''
        self.detector.processing.use_background = False
        self.detector.processing.use_flatfield = False
            
    
    def take_dark(self):
        
        self.__dark_image = None
        
        file_format = self.detector.saving.file_format
        self.detector.saving.file_format = "EDF"
        
        try:
            use_background = self.detector.processing.use_background
            use_flatfield  = self.detector.processing.use_flatfield
            
            self.detector.processing.use_background = False
            self.detector.processing.use_flatfield = False
            
            # take dark image without any correction!
            
            shutter_open = self.shutter.is_open
            if shutter_open:
                print ("close shutter")
                self.shutter.close()
            
            limatake(self.exposure_time, 1, self.detector, save=True)
            
            if shutter_open:
                print ("open shutter")
                self.shutter.open()
            
            self.__dark_image = self.detector.proxy.saving_directory + "/" + self.detector.proxy.saving_prefix + \
                          "{:>04}".format(self.detector.proxy.last_image_saved) + self.detector.proxy.saving_suffix
        except:
            self.detector.saving.file_format = file_format
            raise
        
        self.detector.saving.file_format = file_format
        
        self.detector.processing.background = self.__dark_image
        print(f"Background image = {self.__dark_image}")
        
        # set back the original correctction state
        self.detector.processing.use_background = use_background
        self.detector.processing.use_flatfield = use_flatfield

          
    def take_flat(self):
        
        self.__flat_image = None
        
        file_format = self.detector.saving.file_format
        self.detector.saving.file_format = "EDF"
        
        try:
            use_background = self.detector.processing.use_background
            use_flatfield  = self.detector.processing.use_flatfield
            
            self.detector.processing.use_background = False
            self.detector.processing.use_flatfield = False
            
            # take reference image
            shutter_open = self.shutter.is_open
            if shutter_open == False:
                print ("open shutter")
                self.shutter.open()
            
            limatake(self.exposure_time, 1, self.detector, save=True)
            
            if shutter_open == False:
                print ("close shutter")
                self.shutter.close()
            
            self.__flat_image = self.detector.proxy.saving_directory + "/" + self.detector.proxy.saving_prefix + \
                          "{:>04}".format(self.detector.proxy.last_image_saved) + self.detector.proxy.saving_suffix
        except:
            self.detector.saving.file_format = file_format
            raise
        
        self.detector.saving.file_format = file_format
        
        self.detector.processing.flatfield  = self.__flat_image
        print(f"Flatfield  image = {self.__flat_image}")
        
        # set back the original correctction state
        self.detector.processing.use_background = use_background
        self.detector.processing.use_flatfield = use_flatfield
            
    
    def dark_on(self):
        if self.detector.processing.background == None or self.detector.processing.background == '':
            raise RuntimeError("Cannot switch on! Missing image for dark")
             
        self.detector.processing.use_background = True
    
          
    def dark_off(self):
        self.detector.processing.use_background = False
       
        # be sure that it is applied
        ct(self.exposure_time)
    
    
    def flat_on(self, normalize=True):
        if self.detector.processing.flatfield == None or self.detector.processing.flatfield == '':
            raise RuntimeError("Cannot switch on! Missing image for flat")
        
        # set normaization
        ff_proxy = self.detector._get_proxy("flatfield")
        ff_proxy.normalize = normalize     
        
        # start 
        self.detector.processing.use_flatfield = True
    
    
    def flat_off(self):
       self.detector.processing.use_flatfield = False
       
       # be sure that it is applied
       ct(self.exposure_time)
       
       
    def __info__(self):
        background = 'ON'
        flatfield  = 'ON'
        
        info_str = (f"detector     : {self.detector.name}\n")
        info_str += (f"shutter      : {self.shutter.name}\n")
        info_str += (f"exposure_time: {self.exposure_time}\n")
        info_str += (f"dark_image   : {self.detector.processing.background}\n")
        info_str += (f"flat_image   : {self.detector.processing.flatfield}\n")
        
        if self.detector.processing.use_background == False:
            background = 'OFF'
        if self.detector.processing.use_flatfield == False:
            flatfield  = 'OFF'
        info_str += (f"use_background_substraction : {background}\n")
        info_str += (f"use_flatfield               : {flatfield }\n")
        return info_str
        
