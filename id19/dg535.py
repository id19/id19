
import time
import sys

from bliss.common.tango import DevFailed
from bliss.comm.util import get_comm


class dg535:
    def __init__(self, name, config):
        self.__config = config
        
        self._cnx = get_comm(config)
        
        # reset all latched bits of the intrument status
        status = int(self._query("IS"))
    
    #
    #     Delay generator commands
    #
        
    def get_trigger_mode(self):
        trigger_mode = int(self._query("TM"))
        if trigger_mode == 0:
           status="internal" 
        if trigger_mode == 1:
            status="external" 
        if trigger_mode == 2:
            status="single" 
        if trigger_mode == 3:
            status="burst"
        return status
        
    def set_trigger_mode(self, mode_in):
        if mode_in == "internal":
            mode = 0
        elif mode_in == "external":
            mode = 1
        elif mode_in == "single":
            mode = 2
        elif mode_in == "burst":
            mode = 3
        else:
           raise RuntimeError("Unknown trigger mode!") 
         
        cmd = "TM " + str(mode)
        self._write(cmd) 
        
    def execute_single_shot_trigger(self):
        self._write("SS")
        
    def get_delay (self, channel_in):
        channel = self.__encode_channel(channel_in)
        
        cmd = "DT " + str(channel)
        delay = self._query(cmd)
        l = delay.split(",")
        l[0] = int(l[0])
        l[1] = float(l[1])
        
        l[0] = self.__decode_channel(l[0])
        return l
        
    def set_delay(self, channel_in, ref_channel_in, delay):
        channel = self.__encode_channel(channel_in)
        ref_channel = self.__encode_channel(ref_channel_in)
        
        cmd = "DT " + str(channel) + "," + str(ref_channel) + "," + str(delay)
        self._write(cmd)
        
    def get_scan_meta_data(self):
        meta_data = dict()
        meta_data["trigger_mode"] = self.get_trigger_mode()
        delay = self.get_delay("A")
        delay[1] = str(delay[1])
        meta_data["delay_channel_A"] = delay
        delay = self.get_delay("B")
        delay[1] = str(delay[1])
        meta_data["delay_channel_B"] = delay
        delay = self.get_delay("C")
        delay[1] = str(delay[1])
        meta_data["delay_channel_C"] = delay
        delay = self.get_delay("D")
        delay[1] = str(delay[1])
        meta_data["delay_channel_D"] = delay
        #meta_data["delay_channel_A"] = self.get_delay("A")
        #meta_data["delay_channel_B"] = self.get_delay("B")
        #meta_data["delay_channel_C"] = self.get_delay("C")
        #meta_data["delay_channel_D"] = self.get_delay("D")
        
        scan_info = {"instrument":{"dg535": meta_data}}
        return scan_info
        
        
    #
    #   Communication and infrastructure
    #    
        
    def reset(self):
        print ("\nClean-up the GPIB reading buffer!\n")
        
        #empty the GPIB buffer
        try:
            b = self._cnx.raw_read()
        except (DevFailed, AttributeError) as err:
            # timeout, buffer is empty
            pass
    
    
    def _query(self, msg, **keys):
        send_message = msg + "\r\n"
        
        try:
            raw_reply = self._cnx.write_readline(send_message.encode(), **keys)
            time.sleep(0.1)
            raw_reply = raw_reply.decode()
            
            self._check_error()
            return raw_reply
        
        except (DevFailed, AttributeError) as err:
            self._check_error()
            raise err
            

    def _write(self, msg, **keys):
        send_message = msg + "\r\n"
        
        self._cnx.write(send_message.encode(), **keys)
        self._check_error()
        return
    
        
    def _check_error(self):
        send_message = "ES\r\n"
        
        raw_reply = self._cnx.write_readline(send_message.encode())
        raw_reply = raw_reply.decode()
        
        error_byte = int(raw_reply)
        error_str = ""
        if (error_byte & 1) != 0:
            error_str+="Unrecognized command\n"
        if (error_byte & 2) != 0:
            error_str+="Wrong number of parameters\n"
        if (error_byte & 4) != 0:
            error_str+="Value is outside allowed range\n"
        if (error_byte & 8) != 0:
            error_str+="Wrong mode for the command\n"
        if (error_byte & 16) != 0:
            error_str="Delay linkage error\n"
        if (error_byte & 32) != 0:
            error_str="Delay range error\n"
        if (error_byte & 64) != 0:
            error_str="Recalled data was corrupt\n"
        
        if error_str != "":    
            print(error_str)
    
        
    def __info__(self):
        status = ""
        status += "Trigger Modes:\n"
        status += "internal(0), external(1), single(2), burst(3)\n"
        status += "Current Trigger Mode: "
        trigger_mode = self.get_trigger_mode()
        status += trigger_mode + "\n"
        
        status += "Delay Channel A:\n"
        delay = self.get_delay("A")
        status+= str(delay) + "\n"
        
        status += "Delay Channel B:\n"
        delay = self.get_delay("B")
        status+= str(delay) + "\n"
        
        status += "Delay Channel C:\n"
        delay = self.get_delay("C")
        status+= str(delay) + "\n"
        
        status += "Delay Channel D:\n"
        delay = self.get_delay("D")
        status+= str(delay) + "\n"
        status += "\n"
            
        status_byte = int(self._query("IS"))
        status += "Status Byte:\n"
        if status_byte == 0:
            status += "No information available in the status byte (IS)\n"
        else:
            if (status_byte & 1) != 0:
                status+="Command error detected\n"
            if (status_byte & 2) != 0:
                status+="Busy with timing cycle\n"
            if (status_byte & 4) != 0:
                status+="Trigger has occured\n"
            if (status_byte & 8) != 0:
                status+="80MHz PLL is unlocked\n"
            if (status_byte & 16) != 0:
                status+="Trigger rate to high\n"
            if (status_byte & 64) != 0:
                status+="Service request\n"
            if (status_byte & 128) != 0:
                status+="Memory contents corrupted\n"
        
        return status
        
    
    def __decode_channel(self, channel):
        if channel == 1:
            channel = "T"
        elif channel == 2:
            channel = "A"
        elif channel == 3:
            channel = "B"
        elif channel == 5:
            channel = "C"
        elif channel == 6:
            channel = "D"
        else:
            raise RuntimeError("Unknown channel name!") 
        return channel
            
    def __encode_channel(self, channel):
        if channel == "T":
            channel = 1
        elif channel == "A":
            channel = 2
        elif channel == "B":
            channel = 3
        elif channel == "C":
            channel = 5
        elif channel == "D":
            channel = 6
        else:
            raise RuntimeError("Unknown channel name!") 
        return channel
    
