
import time
import sys

from bliss.common.tango import DevFailed
from bliss.comm.util import get_comm


class dg645:
    def __init__(self, name, config):
        self.__config = config
        
        self._cnx = get_comm(config)
        
        # reset all latched bits of the intrument status
        status = self._query("*IDN?")
        print (status)
        
    
    #
    #     Delay generator commands
    #
    @property    
    def trigger_source(self):
        trigger_mode = int(self._query("TSRC?"))
        if trigger_mode == 0:
           status="internal (0)" 
        if trigger_mode == 1:
            status="external rising edges (1)" 
        if trigger_mode == 2:
            status="external falling edges (2)" 
        if trigger_mode == 3:
            status="single shot external rising edges (3)"
        if trigger_mode == 4:
            status="single shot external falling edges (4)"
        if trigger_mode == 5:
            status="single shot (5)"
        if trigger_mode == 6:
            status="line (6)"
        return status
        
    @trigger_source.setter
    def trigger_source(self, source_in):
        if source_in < 0 or source_in > 6:
            raise RuntimeError("Unknown trigger mode!") 
        else:
            cmd = "TSRC" + str(source_in)
            self._write(cmd) 
        
    
    @property    
    def trigger_level(self):
        trigger_level = float(self._query("TLVL?"))
        return trigger_level
        
    @trigger_level.setter
    def trigger_level(self, level_in):
        cmd = "TLVL" + str(level_in)
        self._write(cmd)
        
    
    @property    
    def trigger_rate(self):
        trigger_rate = float(self._query("TRAT?"))
        return trigger_rate
        
    @trigger_rate.setter
    def trigger_rate(self, rate_in):
        cmd = "TRAT" + str(rate_in)
        self._write(cmd)
    
    

    def get_delay (self, channel_in):
        channel = self.__encode_channel(channel_in)
        
        cmd = "DLAY?" + str(channel)
        delay = self._query(cmd)
        l = delay.split(",")
        l[0] = int(l[0])
        l[1] = float(l[1])
        
        l[0] = self.__decode_channel(l[0])
        return l
        
    def set_delay(self, channel_in, ref_channel_in, delay):
        channel = self.__encode_channel(channel_in)
        ref_channel = self.__encode_channel(ref_channel_in)
        
        cmd = "DLAY " + str(channel) + "," + str(ref_channel) + "," + str(delay)
        self._write(cmd)
        
        
    def get_link (self, channel_in):
        channel = self.__encode_channel(channel_in)
        
        cmd = "LINK?" + str(channel)
        link_channel = int(self._query(cmd))
        
        link_channel_out = self.__decode_channel(link_channel)
        return link_channel_out
        
    def set_link(self, channel_in, link_channel_in):
        channel = self.__encode_channel(channel_in)
        link_channel = self.__encode_channel(link_channel_in)
        
        cmd = "LINK " + str(channel) + "," + str(link_channel)
        self._write(cmd)
        
        
    def get_level_amplitude (self, output_in):
        output = self.__encode_output(output_in)
        
        cmd = "LAMP?" + str(output)
        amplitude = float(self._query(cmd))
        
        return amplitude
        
    def set_level_amplitude(self, output_in, amplitude_in):
        output = self.__encode_output(output_in)
        
        cmd = "LAMP " + str(output) + "," + str(amplitude_in)
        self._write(cmd)
        
        
    def get_level_offset (self, output_in):
        output = self.__encode_output(output_in)
        
        cmd = "LOFF?" + str(output)
        offset = float(self._query(cmd))
        
        return offset
        
    def set_level_offset(self, output_in, offset_in):
        output = self.__encode_output(output_in)
        
        cmd = "LOFF " + str(output) + "," + str(offset_in)
        self._write(cmd)
        
        
    def get_level_polarity (self, output_in):
        output = self.__encode_output(output_in)
        
        cmd = "LPOL?" + str(output)
        polarity = int(self._query(cmd))
        
        return polarity
        
    def set_level_polarity(self, output_in, polarity_in):
        output = self.__encode_output(output_in)
        
        cmd = "LPOL " + str(output) + "," + str(polarity_in)
        self._write(cmd)
        
        
    def get_scan_meta_data(self):
        meta_data = dict()
        meta_data["trigger_mode"] = self.trigger_source
        meta_data["trigger_rate"] = self.trigger_rate
        meta_data["trigger_level"] = self.trigger_level
        delay = self.get_delay("A")
        delay[1] = str(delay[1])
        meta_data["delay_channel_A"] = delay
        delay = self.get_delay("B")
        delay[1] = str(delay[1])
        meta_data["delay_channel_B"] = delay
        delay = self.get_delay("C")
        delay[1] = str(delay[1])
        meta_data["delay_channel_C"] = delay
        delay = self.get_delay("D")
        delay[1] = str(delay[1])
        meta_data["delay_channel_D"] = delay
        delay = self.get_delay("E")
        delay[1] = str(delay[1])
        meta_data["delay_channel_E"] = delay
        delay = self.get_delay("F")
        delay[1] = str(delay[1])
        meta_data["delay_channel_F"] = delay
        delay = self.get_delay("G")
        delay[1] = str(delay[1])
        meta_data["delay_channel_G"] = delay
        delay = self.get_delay("H")
        delay[1] = str(delay[1])
        meta_data["delay_channel_H"] = delay
        
        scan_info = {"instrument":{"dg645": meta_data}}
        return scan_info
        
        
    #
    #   Communication and infrastructure
    #    
        
    def reset(self):
        print ("\nClean-up the GPIB reading buffer!\n")
        
        #empty the GPIB buffer
        try:
            b = self._cnx.raw_read()
        except (DevFailed, AttributeError) as err:
            # timeout, buffer is empty
            pass
    
    
    def _query(self, msg, **keys):
        send_message = msg + "\r"
        
        try:
            raw_reply = self._cnx.write_readline(send_message.encode(), **keys)
            time.sleep(0.1)
            raw_reply = raw_reply.decode()
            
            self._check_error()
            return raw_reply
        
        except (DevFailed, AttributeError) as err:
            self._check_error()
            raise err
            

    def _write(self, msg, **keys):
        send_message = msg + "\r"
        
        self._cnx.write(send_message.encode(), **keys)
        self._check_error()
        return
    
        
    def _check_error(self):
        send_message = "*ESR?\r"
        
        raw_reply = self._cnx.write_readline(send_message.encode())
        raw_reply = raw_reply.decode()
        
        error_byte = int(raw_reply)
        error_str = ""
        if (error_byte & 1) != 0:
            error_str+="Operation complet"
        if (error_byte & 2) != 0:
            error_str+="Reserved"
        if (error_byte & 4) != 0:
            error_str+="Query error"
        if (error_byte & 8) != 0:
            error_str+="Device dependent error"
        if (error_byte & 16) != 0:
            error_str+="Execution error"
        if (error_byte & 32) != 0:
            error_str="Command error"
        if (error_byte & 64) != 0:
            error_str="Reserved"
        if (error_byte & 128) != 0:
            error_str="Power on"
        
        if error_str != "":    
            print(error_str)
            
            send_message = "LERR?\r"
            raw_reply = self._cnx.write_readline(send_message.encode())
            raw_reply = raw_reply.decode()
        
            error_code = int(raw_reply)
            print ("Returned error code : %s \n" % error_code)
    
        
    def __info__(self):
        status = ""
        status += "Trigger Sources:\n"
        status += "\t internal(0),\n\t external rising edges(1),\n\t external falling edges(2),\n\t single shot external rising edges(3), \
                   \n\t single shot external falling edges(4),\n\t single shot (5),\n\t line (6) \n"
        status += "Current Trigger Source: "
        status += self.trigger_source + "\n"
        
        status += "Trigger Level: "
        status += str(self.trigger_level) + "\n"
        
        status += "Trigger Rate: "
        status += str(self.trigger_rate) + "\n\n"
        
        status += "Delay Channel A:\n"
        delay = self.get_delay("A")
        status+= str(delay) + "\n"
        
        status += "Delay Channel B:\n"
        delay = self.get_delay("B")
        status+= str(delay) + "\n"
        
        status += "Delay Channel C:\n"
        delay = self.get_delay("C")
        status+= str(delay) + "\n"
        
        status += "Delay Channel D:\n"
        delay = self.get_delay("D")
        status+= str(delay) + "\n"
        
        status += "Delay Channel E:\n"
        delay = self.get_delay("E")
        status+= str(delay) + "\n"
        
        status += "Delay Channel F:\n"
        delay = self.get_delay("F")
        status+= str(delay) + "\n"
        
        status += "Delay Channel G:\n"
        delay = self.get_delay("G")
        status+= str(delay) + "\n"
        
        status += "Delay Channel H:\n"
        delay = self.get_delay("H")
        status+= str(delay) + "\n"
        status += "\n"
            
        #status_byte = int(self._query("IS"))
        #status += "Status Byte:\n"
        #if status_byte == 0:
            #status += "No information available in the status byte (IS)\n"
        #else:
            #if (status_byte & 1) != 0:
                #status+="Command error detected\n"
            #if (status_byte & 2) != 0:
                #status+="Busy with timing cycle\n"
            #if (status_byte & 4) != 0:
                #status+="Trigger has occured\n"
            #if (status_byte & 8) != 0:
                #status+="80MHz PLL is unlocked\n"
            #if (status_byte & 16) != 0:
                #status+="Trigger rate to high\n"
            #if (status_byte & 64) != 0:
                #status+="Service request\n"
            #if (status_byte & 128) != 0:
                #status+="Memory contents corrupted\n"
        
        return status
        
    
    def __decode_channel(self, channel):
        if channel == 0:
            channel = "T0"
        elif channel == 1:
            channel = "T1"
        elif channel == 2:
            channel = "A"
        elif channel == 3:
            channel = "B"
        elif channel == 4:
            channel = "C"
        elif channel == 5:
            channel = "D"
        elif channel == 6:
            channel = "E"
        elif channel == 7:
            channel = "F"
        elif channel == 8:
            channel = "G"
        elif channel == 9:
            channel = "H"
        else:
            raise RuntimeError("Unknown channel name!") 
        return channel
            
    def __encode_channel(self, channel):
        if channel == "T0":
            channel = 0
        elif channel == "T1":
            channel = 1
        elif channel == "A":
            channel = 2
        elif channel == "B":
            channel = 3
        elif channel == "C":
            channel = 4
        elif channel == "D":
            channel = 5
        elif channel == "E":
            channel = 6
        elif channel == "F":
            channel = 7
        elif channel == "G":
            channel = 8
        elif channel == "H":
            channel = 9
        else:
            raise RuntimeError("Unknown channel name!") 
        
        return channel
        
    def __encode_output(self, output):
        if output == "T0":
            output = 0
        elif output == "AB":
            output = 1
        elif output == "CD":
            output = 2
        elif output == "EF":
            output = 3
        elif output == "GH":
            output = 4
        else:
            raise RuntimeError("Unknown output name!") 
        
        return output
    
