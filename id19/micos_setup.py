import time
import gevent
import socket
import struct
import gevent
import time

from bliss.comm.util import get_comm, TCP
from bliss.comm.tcp import Command, SocketTimeout
from bliss.shell.standard import umv, mv, umvr, mvr


class MicosSetup():
    def __init__(self, name, config):
        # get motors and controller connection
        self.rotation_motor = config.get("rotation_motor")
        self.tilt_motor     = config.get("tilt_motor")
        self.magnet_motors  = config.get("magnet_motors")
        self.pusher_motors  = config.get("pusher_motors")
        
        try:
            # open the communication
            opt = {'port':6542, 'eol':b'\n', 'timeout':2.0}
            self.comm = get_comm(config, ctype=TCP, **opt)
        
            _, version, _, _ = self.comm.write_readlines(b"\n", 4)
            version = version.decode().strip()
            print (version)
            
        except SocketTimeout:
            raise RuntimeError("\n\tCannot connect to Micos MotionServer on lamino.esrf.fr! \n")
            
    def micos_on(self):
        print("Switching on Micos controllers\n")
        # power on the SAM motors
        cmd = "Sam Power on"
        self.raw_write(cmd)
        # power on the Cont2 motors
        cmd = "Cont2 Power on"
        self.raw_write(cmd)
        
        
        time.sleep(2)
        
        # synch_hard for all motors
        print("Synchronize motors in Bliss\n")
        self.rotation_motor.sync_hard()
        self.tilt_motor.sync_hard()
        self.magnet_motors[0].sync_hard()
        self.magnet_motors[1].sync_hard()
        self.pusher_motors[0].sync_hard()
        self.pusher_motors[1].sync_hard()
        
        
        # Message to home the rl motor
        print ("\n\tDon't forget to home the roataion axis!\n")

        
    def micos_off(self):
        # move tilt to 0
        umv(self.tilt_motor, 0.0)
        
        # move magnet motors to 0
        umv(self.magnet_motors[0], 0.0, self.magnet_motors[1], 0.0)
        
        # move pusher motors to 0
        umv(self.pusher_motors[0], 0.0, self.pusher_motors[1], 0.0)
        
        # move rotation to 0
        umv(self.rotation_motor, 0.0)
        
        # power off the SAM motors
        cmd = "Sam Power off"
        self.raw_write(cmd)
        # power on the Cont2 motors
        cmd = "Cont2 Power off"
        self.raw_write(cmd)


    def raw_readline(self):
        return self.comm.readline(timeout=2).decode()

    def raw_write(self, cmd):
        self.comm.flush()
        cmd = cmd + "\n"
        self.comm.write(cmd.encode("ascii"))

    def raw_write_read(self, cmd):
        self.comm.flush()
        cmd = cmd + "\n"
        msg  = self.comm.write_readline(cmd.encode("ascii"), timeout=2)
        msg = msg.decode()
        #print("RETURN", msg)
        return msg
