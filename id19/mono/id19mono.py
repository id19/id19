import sys
import gevent
import math
import time
import datetime

from bliss import setup_globals, global_map
from bliss.common import mapping, scans
from bliss.common import session
from bliss.common.scans import dscan, ct
from bliss.common.logtools import log_info, log_debug, log_error 
from bliss.common.cleanup import cleanup, error_cleanup, capture_exceptions
from bliss.scanning.scan_tools import peak, goto_peak
from bliss.shell.standard import umv, umvr, mv, plotselect
from bliss.shell.getval import getval_yes_no
from bliss.config.settings import ParametersWardrobe
from bliss.controllers.tango_shutter import TangoShutterState


from monochromator.monochromator import XtalManager


from enum import Enum
class MonoMode(Enum):
    UNDEFINED = 0
    BB50      = 1
    LL50      = 2
    LL24      = 3
    OUT       = 4
    MM15      = 5
    

class Id19Mono():
    def __init__(self, name, config):
        # init logging
        self.name = name
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")

        self.config = config
        
        # motors
        
        self.X2: self.config["X2"]
        self.Y1: self.config["Y1"]
        self.Y2: self.config["Y2"]
        self.Z1: self.config["Z1"]
        self.Z2: self.config["Z2"]
        self.THY1: self.config["THY1"]
        self.THY2: self.config["THY2"]
        self.slits_vgap: self.config["slits_vgap"]
        
        self.X2_safe_position  = self.config["X2_safe_position"]
        
        self.laue_THY1_offset  = self.config["laue_THY1_offset"]
        self.laue_THY2_offset  = self.config["laue_THY2_offset"]
        
        self.multilayer_Z2_offset  = self.config["multilayer_Z2_offset"]
        
        self.xtal_size          = self.config["xtal_size"]
        self.stabilization_time = self.config["stabilization_time"]
        
        self.beam_stop = self.config["beam_stop"]
        self.diode_pos = self.config["diode_pos"]
        self.diode_cnt = self.config["diode_cnt"]
        self.beam_shutter = self.config["beam_shutter"]
        self.frontend  = self.config["frontend"]
        
        # modes
        
        self.modes = {}
        self.modes[MonoMode.UNDEFINED]={"description" : "Mode is undefined"}
        self.modes[MonoMode.OUT]     = self.config["white_beam"][0]
        self.modes[MonoMode.BB50]    = self.config["bragg_mode"][0]
        self.modes[MonoMode.LL50]    = self.config["laue_mode_50"][0]
        self.modes[MonoMode.LL24]    = self.config["laue_mode_24"][0]
        self.modes[MonoMode.MM15]    = self.config["multilayer"][0]
        
        # parameters
        
        param_defaults = {}
        param_defaults['mode']  = MonoMode.UNDEFINED
        param_defaults['X2_pos']    = 0
        param_defaults['Y1_pos']    = 0
        param_defaults['Y2_pos']    = 0
        param_defaults['Z1_pos']    = 0
        param_defaults['Z2_pos']    = 0
        param_defaults['THY1_pos']  = 0
        param_defaults['THY2_pos']  = 0
        param_defaults['energy']    = 0
        param_defaults['xtal_name'] = None
        
        self.param_name = self.name + ":parameters"
        self.parameters = ParametersWardrobe(self.param_name)
            
        parameters_dict = self.parameters.to_dict(export_properties=True)
        for k,v in param_defaults.items():
            if k not in parameters_dict:
                self.parameters.add(k,v)

        self._mode   = self.parameters.mode
        self._energy = self.parameters.energy
        
        self.mono_xtals = self.config["xtals"]
        self.mono_xtals.xtal_sel = self.parameters.xtal_name
        
    #
    # mode handling
    #    
    @property
    def mode (self):
        self.check_mode()
        return self._mode
    
    @mode.setter
    def mode (self, req_mode):
        self._mode = MonoMode.UNDEFINED
        
        if req_mode == MonoMode.UNDEFINED:
            return
        
        # close FE
        if self.frontend.state not in [TangoShutterState.CLOSED, TangoShutterState.DISABLE, TangoShutterState.STANDBY]:
            print ("Close FrontEnd")
            self.frontend.close()
        
        if req_mode == MonoMode.OUT:
            self.remove()
            return
        
        print ("Move to safe position")
        self.safe_position()
        
        print ("Move in beam stop")
        self.beam_stop.IN()
        
        # for all other modes, get the mode positions
        mono_mode = self.modes[req_mode]
        
        # move crystals and preposition hights (offset)
        
        # for the multilayer crystals, an offset has to be applied to Z2.
        # the second crystal is mounted on a 18.5 mm thick plate.
        if req_mode == MonoMode.MM15:
            z2_pos = mono_mode["beam_offset"]+ mono_mode["Z1_pos"] + self.multilayer_Z2_offset
        else:
            z2_pos = mono_mode["beam_offset"]+ mono_mode["Z1_pos"]
        
        print ("Move Y and Z motors")
        umv(self.Y1, mono_mode["Y1_pos"], 
            self.Y2, mono_mode["Y2_pos"],
            self.Z1, mono_mode["Z1_pos"],
            self.Z2, z2_pos)
        
        # choose the correct xtal for the mode
        if req_mode == MonoMode.MM15:
             self.mono_xtals.xtal_sel = 'W_B4C'
        else:
             self.mono_xtals.xtal_sel = 'Si111'
        print (f"Set xtal to {self.mono_xtals.xtal_sel}")
        
        print ("Move diode in")
        if req_mode == MonoMode.MM15:
            self.diode_pos.IN15()
        else:
            if req_mode == MonoMode.LL24:
                self.diode_pos.IN24()
            else:
                self.diode_pos.IN50()
        
        self._mode   = req_mode
        
        # Store the positions for the applied mode
        self.store_config()
        
            
    def check_mode(self):
        current_mode = self._mode
        self._mode    = MonoMode.UNDEFINED
        
        if current_mode == MonoMode.UNDEFINED:
            return
        
        if current_mode == MonoMode.OUT:
            white_beam = self.modes[MonoMode.OUT]
            
            if self.is_aprox(self.Y1.position, white_beam["Y1_pos"], 5):
                if self.is_aprox(self.Y2.position, white_beam["Y2_pos"], 5):
                    if self.is_aprox(self.Z1.position, white_beam["Z1_pos"], 1):
                        if self.is_aprox(self.Z2.position, white_beam["Z2_pos"], 1):
                            if self.beam_stop.position == 'OUT':
                                self._mode = current_mode
            return
        
        # for all other modes, get the mode positions
        mono_mode = self.modes[current_mode]
            
        if self.is_aprox(self.Y1.position, mono_mode["Y1_pos"], 5):
            if self.is_aprox(self.Y2.position, mono_mode["Y2_pos"], 5):
                if self.beam_stop.position == 'IN':
                    self._mode = current_mode
        return
     
    #
    # last applied configuration
    #                            
        
    def store_config(self):
        self.parameters.X2_pos = self.X2.position
        self.parameters.Y1_pos = self.Y1.position
        self.parameters.Y2_pos = self.Y2.position
        self.parameters.Z1_pos = self.Z1.position
        self.parameters.Z2_pos = self.Z2.position
        self.parameters.THY1_pos = self.THY1.position
        self.parameters.THY2_pos = self.THY2.position
        self.parameters.mode     = self._mode
        self.parameters.energy   = self._energy
        self.parameters.xtal_name = self.mono_xtals.xtal_sel
        
    
    def print_config(self):    
        print("\n---------------------------------------------------------")
        print("mono mode   : %s " % self.modes[self.parameters.mode]["description"])
        print("mono energy : %g " % self.parameters.energy )
        print("xtal name   : %s " % self.mono_xtals.xtal_sel )
        print("------------------- MONO MOTORS -------------------------")
        print("X2 = %5gmm" % self.parameters.X2_pos)
        print("Y1 = %5gmm    Y2 = %5gmm" % (self.parameters.Y1_pos, self.parameters.Y2_pos))
        print("Z1 = %5gmm    Z2(beam gap/offset) = %6.4fmm " % (self.parameters.Z1_pos, self.parameters.Z2_pos))
        print("Crystals Angles : THY1 = %g   THY2 = %g "% (self.parameters.THY1_pos, self.parameters.THY2_pos))
        print("Beam stop = %s" % self.beam_stop.position)
        print("Diode position = %s" % self.diode_pos.position)
        print("----------------------------------------------------------\n")
        
    def __info__(self):
        info_str  = ("---------------------------------------------------------\n")
        info_str += ("mono mode   : %s \n" % self.modes[self.mode]["description"])
        info_str += ("mono energy : %g \n" % self.parameters.energy )
        info_str += ("xtal name   : %s \n" % self.mono_xtals.xtal_sel )
        info_str += ("------------------- MONO MOTORS -------------------------\n")
        info_str += ("X2 = %5gmm \n" % self.X2.position)
        info_str += ("Y1 = %5gmm    Y2 = %5gmm \n" % (self.Y1.position, self.Y2.position))
        info_str += ("Z1 = %5gmm    Z2(beam gap/offset) = %6.4fmm \n" % (self.Z1.position, self.Z2.position))
        info_str += ("Crystals Angles : THY1 = %g   THY2 = %g \n"% (self.THY1.position, self.THY2.position))
        info_str += ("Beam stop = %s \n" % self.beam_stop.position)
        info_str += ("Diode position = %s \n" % self.diode_pos.position)
        info_str += ("----------------------------------------------------------\n")
        return info_str
        
    
    ###############################################################################
    #
    # where energy (we) and where lambda (wl)
    #
    ###############################################################################
    def we(self):
        theta_angle = self.get_theta()

        if self.is_aprox(theta_angle, 0 , 0.01):
            print ("Theta angle < 0.01, cannot calculte energy!")
        else:
            energy = self.mono_xtals.bragg2energy(theta_angle)
            print ("\nEnergy from theta angle: %g keV" % energy)
            print ("Last applied energy    : %g kev \n" % self.parameters.energy)
        
    def wl(self):
        theta_angle = self.get_theta()

        if self.is_aprox(theta_angle, 0 , 0.01):
            print ("Theta angle < 0.01, cannot calculte lambda!")
        else:
            lamda = 2 * 3.1356 * math.sin (math.radians(theta_angle))
            print ("Lambda is %g Angstrom\n\n" % lamda)
        
    ###############################################################################
    #
    # mve    : move to energy, steps in keV
    # mvre   : relative changes in energy !!!!!!!!!!! step in eV !!!!!!!!!!!!
    #
    ###############################################################################
    def mve(self, energy, relative=False):
        #crystals_to_be_moved = True
        X2_pos   = None
        Z1_pos   = None
        Z2_pos   = None
        THY1_pos = None
        THY2_pos = None
        
        # mode check
        self.check_mode()
        if self._mode == MonoMode.UNDEFINED or self._mode == MonoMode.OUT:
            raise RuntimeError("No operation mode applied. The monochromator is not pre-positioned")
            
        #if self._mode == MonoMode.MM15:
        #    raise RuntimeError("The multilayer mode is not yet implemented")
        
        # check energy range
        e_min = self.modes[self._mode]['min_energy']
        e_max = self.modes[self._mode]['max_energy']
        if energy < e_min or energy > e_max:
            raise RuntimeError("Target energy %g keV is outside range [%g keV ;%g keV]" % (energy, e_min, e_max) )
        
        # calculate X2 pos
        
        #### Towers distance and calculated Bragg angle theta  ####
        # Towers distance (X2_pos) is calculated from theorical angle (in
        # degrees) depending on energy.
        # NB : angle_to_distance() needs to know the beam_offset to calculate distance.
        theta_angle = self.mono_xtals.energy2bragg(energy)
        X2_pos = self.angle_to_distance(theta_angle)

        # calculate theta angles
        if self._mode == MonoMode.LL50 or self._mode == MonoMode.LL24:
            THY1_pos = theta_angle + self.laue_THY1_offset
            THY2_pos = theta_angle + self.laue_THY2_offset
        else:
            #if self._mode == MonoMode.BB50:
            THY1_pos = theta_angle
            THY2_pos = theta_angle
        
        # check beam size
        #self.check_beam_size(theta_angle)
        
        # refine Z1 and Z2 pos
        Z1_pos = self.modes[self._mode]["Z1_pos"] 
        
        if self._mode == MonoMode.MM15:
            Z2_pos = self.modes[self._mode]["beam_offset"] + Z1_pos + self.multilayer_Z2_offset
        else:
            Z2_pos = self.modes[self._mode]["beam_offset"] + Z1_pos
        
        
        #
        # this part is some bricolage and needs to be cleaned-up!!!!!!!!!!
        #
        security_margin = 3
        
        X2_high_limit = 1250
        Z1_low_limit  = -28
        Z2_high_limit = 70
        if self.Z2.position < 50:
            X2_low_limit = 350
        else:
            X2_low_limit = 300
        
        
        
        
        if X2_pos >= self.X2_safe_position:
            if X2_pos > X2_high_limit:
                X2_pos = X2_high_limit
        else:
            # risky movement, refining the towers heights
            beam_vertical_size   = self.slits_vgap.position
            space_available      = 0.5 * self.xtal_size - (security_margin / math.sin(math.radians(theta_angle)))
            half_beam_projection = 0.5 * beam_vertical_size / (math.sin(math.radians(theta_angle)))
            
            # ######## Attention!!!! half_beam_projection > space_available (because of secu_margin along z)!
            # ######## !!!!! the absolute value of the top_beam_place must be used
            top_beam_place = math.fabs(space_available - half_beam_projection)
            
            print("beam_vertical_size   = %g" % beam_vertical_size)
            print("space_available      = %g" % space_available)
            print("half_beam_projection = %g" % half_beam_projection)
            print("top_beam_place       = %g" % top_beam_place)
            print("security_margin      = %g \n " % security_margin)
            
            if top_beam_place > 0.0:
                Z1_calc = -1 * top_beam_place * math.sin(math.radians(theta_angle))
            else:
                Z1_calc = 0.0
            
            Z1_pos = Z1_calc
            if Z1_calc < Z1_low_limit:
                Z1_pos = Z1_low_limit
                
            horizontal_displacement_xtal = top_beam_place * math.cos(math.radians(theta_angle))   # ZU = SU * cos(Theta_Angle)
            print( "horizontal_displacement_xtal = %g   X2_pos = %g  X2_low_limit= %g  \n" % 
                 (horizontal_displacement_xtal, X2_pos, X2_low_limit) )

            X2_calc = X2_pos + horizontal_displacement_xtal
            if  X2_calc < X2_low_limit:
                X2_calc = X2_low_limit
            print( "X2_calc = %g \n" % X2_calc)
            
            if X2_calc == 300:
                # risky config -> X2 limited to 300
                print("necessary to move Z2 \n")
                
                Z2_correction =  (X2_calc - (X2_pos + horizontal_displacement_xtal)) * math.tan(math.radians(2*theta_angle))
                Z2_calc = Z2_pos +  Z2_correction
                if Z2_calc > Z2_high_limit:
                    Z2_pos = Z2_lim
                else:
                    Z2_pos = Z2_calc
                
            if X2_calc >= self.X2_safe_position:
                # In this case, no need to move beam to the border of the crystal
                # Force X2 back to 400
                # and calculate the new corresponding Z1 movement.

                # calc the distance between the new X2 and the safety value of X2 that is 400
                X2_from_safety = X2_calc - self.X2_safe_position

                # calc how much to move down the 1st crystal in case X2 is fixed at 400
                Z1_pos = -X2_from_safety * math.tan(math.radians(theta_angle))
                X2_pos = self.X2_safe_position
            else:
                X2_pos = X2_calc
                    
        # print calculated positions
        print("\n---------------------------------------------------------")
        print("mono mode   : %s " % self.modes[self.parameters.mode]["description"])
        print("mono energy : %g " % energy)
        print("------------------- Current Motor Position--------------")
        print("X2 = %5gmm" % self.X2.position)
        print("Y1 = %5gmm    Y2 = %5gmm" % (self.Y1.position, self.Y2.position))
        print("Z1 = %5gmm    Z2(beam gap/offset) = %6.4fmm " % (self.Z1.position, self.Z2.position))
        print("Crystals Angles : THY1 = %g   THY2 = %g "% (self.THY1.position, self.THY2.position))
        print("Beam stop = %s" % self.beam_stop.position)
        print("------------------- Calculated Motor Position--------------")
        print("X2 = %5gmm" % X2_pos)
        print("Z1 = %5gmm    Z2(beam gap/offset) = %6.4fmm " % (Z1_pos, Z2_pos))
        print("Crystals Angles : THY1 = %g   THY2 = %g "% (THY1_pos, THY2_pos))
        print("----------------------------------------------------------\n")
        if not getval_yes_no("Do you want to move?",default="yes" ):
            return
            
        # close beam shutter
        if self.beam_shutter.state not in [TangoShutterState.CLOSED, TangoShutterState.DISABLE]:
            print ("Close beam shutter")
            self.beam_shutter.close() 
        
        # For security move Y2 to the middle and move it back after the X2 movement
        # Only do this if X2 > 330 !!!!!!!!
        move_y2 = False
        # don't move in the middle for relative movements
        if relative == False:
            if self.X2.position > self.X2_safe_position or X2_pos > self.X2_safe_position:
                # Move in the middle only if X2 has to move
                if not self.is_aprox(self.X2.position, X2_pos, 0.1):
                    Y2_act_pos = self.Y2.position
                    move_y2 = True
            
        # determine the order of movementes in critical positions
        move_to_lower_energy = True
        #if self.calc_energy() > e_min:
        if self.energy > e_min:
            #if self.calc_energy() < energy:
            if self.energy < energy:
                 move_to_lower_energy = False
            print (move_to_lower_energy)
        
        # The movement order depend on the direction we want to move!
        if move_to_lower_energy:
            # Be sure to have no theta angle applied when moving into the dangerous zone!
            # For relative movements, we will not move to 0
            if relative == False and X2_pos < self.X2_safe_position:
                umv(self.THY1, 0.0, 
                    self.THY2, 0.0)
                self._energy = 0
            
            # move Z
            umv(self.Z1, Z1_pos, 
                self.Z2, Z2_pos)
            
            
            if move_y2 == True:
                print("\nMove monochromator sledge in the middle to avoid collisions!")
                umv (self.Y2, 90)        # move in the middle, to avoid collissions
            # move X2
            umv(self.X2, X2_pos) 
            if move_y2 == True:
                print(f"\nMove monochromator sledge back to the mode position!")
                umv (self.Y2, Y2_act_pos) #move back to mono mode position
            
            # move Theta
            umv(self.THY1, THY1_pos, 
                self.THY2, THY2_pos)
        
        # move to higher energy
        else:
            # move Theta
            umv(self.THY1, THY1_pos, 
                self.THY2, THY2_pos)
            
            if move_y2 == True:
                umv (self.Y2, 90)  # move in the middle
            # move X2
            umv(self.X2, X2_pos) 
            if move_y2 == True:
                umv (self.Y2, Y2_act_pos)
            
            # move Z
            umv(self.Z1, Z1_pos, 
                self.Z2, Z2_pos)
        
        self._energy = energy
        print ("\nMonochromator in %s mode\nwith an energy of %g keV\n" % (self.modes[self._mode]["description"],energy))
        
        #
        # Store the positions for the applied energy
        #
        self.store_config()
        
        
        # wait for stabilization
        print ("Waiting %g seconds for stabilisation" % self.stabilization_time)
        time.sleep (self.stabilization_time)
        
        # open beam shutter
        print ("Open beam shutter")
        self.beam_shutter.open()
        
        # tune
        self.tune()
        
        # fine tune
        self.fine_tune()
        
        
    
    def mvre(self, delta_energy):
        if math.fabs(delta_energy) > 2000:
            raise ValueError("Delta energy (eV) is too large! Has to be smaller than 2000eV!")
        
        if math.fabs(delta_energy) < 0.01:
            raise ValueError("Delta energy (eV) is too small! Has to be bigger than 0.01eV!")
        
        theta_angle = self.get_theta()

        old_e = self.mono_xtals.bragg2energy(theta_angle) * 1000   # conversion to eV
        new_e = (old_e + delta_energy) / 1000           # in keV

        print ("Old energy = %g \t new energy = %g" % ((old_e/1000), new_e) )
        self.mve(new_e, relative = True)
    
    #
    # rocking curve scans
    #
    def tune(self):
        ct_diode = 0.01
        # Crystal 1 rocking curve:  
        plotselect (self.diode_cnt)
        
        if self._mode == MonoMode.BB50:
            dscan (self.THY1, -0.06, 0.01, 500, ct_diode, save=False)      #Bragg scan
        else:
            if self._mode == MonoMode.LL50:
                dscan (self.THY1, -0.01, 0.06, 500, ct_diode, save=False)       #Laue50 scan
            else:
                dscan (self.THY1, -0.05, 0.05, 500, ct_diode, save=False)       #default
                
        goto_peak()
        time.sleep(2)
        
        
    def fine_tune(self):
        ct_diode = 0.01
        plotselect (self.diode_cnt)
        
        if self._mode == MonoMode.MM15:
            dscan (self.THY1, 0.0025, -0.0025, 100, ct_diode, save=False)       #Multilayer scan
        else:
            dscan (self.THY1, -0.0025, 0.0025, 100, ct_diode, save=False)       #default
        
        goto_peak()
        time.sleep(1)
        
        umvr (self.THY1, 0.0003)
        time.sleep(2)
        
    
    def angle_to_distance(self, angle):
        if self._mode == MonoMode.UNDEFINED or self._mode == MonoMode.OUT:
            raise ValueError ("No valid monochromator mode applied!")
            
        beam_gap = self.modes[self._mode]["beam_offset"]
        distance = beam_gap / math.tan(2 * math.radians(angle))
        return distance    
    
        
    def check_beam_size(self, theta_angle):
        beam_vertical_size = self.slits_vgap.position  # mm
        half_beam_projection = 0.5 * beam_vertical_size / (math.sin(math.radians(theta_angle)))

        if half_beam_projection > (0.5 * self.xtal_size):
            # Calculation of the maximum vertical beam size

            print ("theta_angle = %s" % theta_angle)

            v_beam_ok = self.xtal_size * math.sin(math.radians(theta_angle))

            if beam_vertical_size > v_beam_ok:
                print("Vertical beam size %g mm is too large!" % beam_vertical_size)
                print("Max vertical beam size is: %g mm \n" % v_beam_ok)
                print("Reduce the vertical gap of the secondary slits accordingly! \n")
        
            raise ValueError ("Vertical beam size is too large!")   
        
        
    def remove(self):
        # close FE
        #if self.frontend.state not in [TangoShutterState.CLOSED, TangoShutterState.DISABLE, TangoShutterState.STANDBY]:
            #print ("Close FrontEnd")
            #self.frontend.close()
        
        # remove diode
        print ("Remove diode")
        self.diode_pos.OUT()
        
        # set the mode
        print ("Move mono to white beam position")
        
        self._mode = MonoMode.UNDEFINED
        
        #
        # get white beam positions
        #
        white_beam = self.modes[MonoMode.OUT]
        
        print ("Move to safe position")
        self.safe_position()
        
        print ("Move Y and Z motors")
        umv(self.Y1, white_beam["Y1_pos"], 
            self.Y2, white_beam["Y2_pos"],
            self.Z1, white_beam["Z1_pos"], 
            self.Z2, white_beam["Z2_pos"])
        
        # move out beam stop
        self.beam_stop.OUT()
        
        self._mode   = MonoMode.OUT
        self._energy = 0
        self.mono_xtals.xtal_sel = None
        print ("Monochromator in white beam position")
        
        #
        # Store the positions for the applied mode
        #
        self.store_config()
        
        
    def safe_position(self):
        print ("Move THY1 and THY2 to 0")
        umv(self.THY1, 0.0, 
            self.THY2, 0.0)
        self._energy = 0
        
        print ("Move X2 to save position")
        if self.X2.position < self.X2_safe_position:
            umv(self.X2, self.X2_safe_position + 10)   # with a little margin

    
    def get_theta (self):
        if self._mode == MonoMode.LL50 or self._mode == MonoMode.LL24:
            theta_angle = self.THY1.position - self.laue_THY1_offset
        else:
            theta_angle = self.THY1.position 
        return theta_angle

    def is_aprox(self, x, y, tol):
        if math.fabs(x - y) <= tol:
            return True
        else:
            return False
    
    @property
    def energy(self):
        try:
            energy = self.mono_xtals.bragg2energy(self.get_theta())
        except ZeroDivisionError:
            # multilayer returns a division by zero exception the theta = 0!
            energy = math.inf
        
        return energy
    
    @property
    def lamda(self):
        return (2 * 3.1356 * math.sin (math.radians(self.get_theta())))
