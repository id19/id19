import math

from bliss.physics.units import ur
from bliss.physics.diffraction import CrystalPlane

"""
- class: DCMenergy
  # Fix exit geometry
  dist_fix_exit: 0.01
  dist_xtal_fjs0: 25
  # Crystals parameters
  xtal1: Si111
  xtal2: Si311
"""

class MonochromatorBase(object):
    
    
    def __init__(self, name, config):
        
        # keep config node
        self.config_node = config
        
        ## Fix exit Geometry
        #self.fix_exit_offset = self.config_node.get("fix_exit_offset")
        self.fix_exit_offset = None
        self.xtal_sel        = None
        
        # Crystals parameters
        xtals = self.config_node.get("xtals")
        self.xtal_names = []
        self.xtal = {}
        for xtali in range(len(xtals)):
            name = xtals[xtali]["xtal_name"]
            self.xtal_names.append(name)
            self.xtal[name] = CrystalPlane.fromstring(name)
                

    ########################
    #
    # Fix Exit Monochromator
    #
    @property
    def fix_exit(self):
        if self.fix_exit_offset == None:
            raise RuntimeError("Monochromator Base: No fix exit offset given!")
            
        return self.fix_exit_offset
        
    @fix_exit.setter
    def fix_exit(self, value):
        self.fix_exit_offset = value
        #self.config_node["fix_exit_offset"] = value
        #self.config_node.save()
        
    def set_crystal(self, crystal):
        if crystal in self.xtal_names:
            self.xtal_sel = crystal
        else:
            self.xtal_sel = None
            raise RuntimeError("Monochromator Base: Crystal (%s) not configured" % crystal)
            
    def get_crystal(self):
        if self.xtal_sel == None:
            raise RuntimeError("Monochromator Base: No crystal selected!")
            
        return self.xtal_sel

    #####################
    #
    # Calculation methods
    #
    def energy2bragg(self, ene):
        if self.xtal_sel == None:
            raise RuntimeError("Monochromator Base: No crystal selected!")
            
        xtal = self.xtal[self.xtal_sel]
        bragg = xtal.bragg_angle(ene*ur.keV).to(ur.deg).magnitude
        return(bragg)
        
    def bragg2energy(self, bragg):
        if self.xtal_sel == None:
            raise RuntimeError("Monochromator Base: No crystal selected!")
            
        xtal = self.xtal[self.xtal_sel]
        energy = xtal.bragg_energy(bragg*ur.deg).to(ur.keV).magnitude
        return energy
        
    def bragg2dxtal(self, bragg):
        if self.fix_exit_offset == None:
            raise RuntimeError("Monochromator Base: No fix exit offset given!")
            
        dxtal = self.fix_exit_offset / (2.0 * math.cos(math.radians(bragg)))
        return dxtal
        
    def energy2dxtal(self, ene):
        if self.fix_exit_offset == None:
            raise RuntimeError("Monochromator Base: No fix exit offset given!")
            
        bragg = xtal.bragg_angle(ene*ur.keV).to('deg').magnitude
        dxtal = self.fix_exit_offset / (2.0 * math.cos(math.radians(bragg)))
        return dxtal
