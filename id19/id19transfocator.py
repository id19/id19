
from bliss.controllers.transfocator import Transfocator
from bliss.shell.standard import umv, mv, umvr, mvr

class ID19Transfocator(Transfocator):
    
    def __init__(self, name, config):
        self.name = name
        
        self.slit_gaps          = config.get("slit_gaps")
        self.slit_gap_positions = config.get("slit_gap_positions")
        self.slit_offsets          = config.get("slit_offsets")
        self.slit_offset_positions = config.get("slit_offset_positions")
        
        # Initialise the Transfocator class
        super().__init__(name, config)


    def close_slits(self):
        # move slit offsets when configured
        if self.slit_offsets != None:
            print ("Move primary slit offsets")
            for index, item in enumerate(self.slit_offsets):
                umv (item, self.slit_offset_positions[index])
        
        # move slits gaps only when necessary
        if self.slit_gaps != None:
            print ("Move primary slit gaps when necessary")
            for index, item in enumerate(self.slit_gaps):
                if item.dial > self.slit_gap_positions[index]:
                    umv (item, self.slit_gap_positions[index])
                
        #if self.slit_gaps[0].position > self.slit_gap_positions[0] and \
           #self.slit_gaps[1].position > self.slit_gap_positions[1]:
            #print ("Move primary slit gaps")
            #umv (self.slit_gaps[0], self.slit_gap_positions[0], self.slit_gaps[1], self.slit_gap_positions[1])
        #else:
            #pass
            ##print("Slits are closed!")


    def set_n(self, *idx_values):
        """ Set the lenses. Check if there is a security pinhole to be set.
            To be used by __setitem__()
        Args:
            (list): Lense index, lens value
        """
        bits = self.pos_read()
        for idx, value in zip(idx_values[::2], idx_values[1::2]):
            if value is None or idx in self.empty_jacks:
                continue
            else:
                if self._encode(value):
                    bits |= 1 << idx
                else:
                    bits &= 0xFFFFFFFF ^ (1 << idx)
        if self.safety and bits and self.pinhole:
            for pinhole in self.pinhole:
                bits |= 1 << pinhole
        if self.pos_read() == bits:
            # nothing to do
            return
            
        # Be sure primary slits are closed
        self.close_slits()
            
        self.tfstatus_set(bits)
            
        
    def set_bitvalue(self, value):
        """ Set bit values checking if there is a security pinhole to set.
        Args:
            (value): lens bit value
        """
        if self.safety and value and self.pinhole:
            for pinhole in self.pinhole:
                value |= 1 << pinhole
        
        # Be sure primary slits are closed
        self.close_slits()        
        
        self.tfstatus_set(value)

    def _encode(self, status):
        if status in (1, "in", "IN", True):
            return True
        if status in (0, "out", "OUT", False):
            return False
        raise ValueError("Invalid position {!r}".format(status))
        
        
        
    def status_dict(self):
        """The status of the transfocator as dictionary
        Returns:
            (dict): Keys are the labels of the lenses, values are True or False
        """
        positions = {}
        value = self.pos_read()
        for i in range(self.nb_lens + self.nb_pinhole):
            if i in self.empty_jacks:
                lbl, position = "X{}", None
            else:
                lbl = "P{}" if i in self.pinhole else "L{}"
                position = value & (1 << i) > 0
                if value & (1 << i + self.nb_lens + self.nb_pinhole) > 0:
                    position = None
            positions[lbl.format(i)] = position
        return positions


    def __len__(self):
        return self.nb_lens + self.nb_pinhole

    def __getitem__(self, idx):
        pos = list(self.status_dict().values())
        if isinstance(idx, int):
            return self._display(pos[idx])
        if isinstance(idx, slice):
            idx = list(range(*idx.indices(self.nb_lens + self.nb_pinhole)))
        return [self._display(pos[i]) for i in idx]

    def __setitem__(self, idx, value):
        if isinstance(idx, int):
            args = idx, value
        else:
            if isinstance(idx, slice):
                idx = list(range(*idx.indices(self.nb_lens + self.nb_pinhole)))
            nb_idx = len(idx)
            if not isinstance(value, (tuple, list)):
                value = nb_idx * [value]
            nb_value = len(value)
            if nb_idx != nb_value:
                raise ValueError(
                    "Mismatch between number of lenses ({}) "
                    "and number of values ({})".format(nb_idx, nb_value)
                )
            args = [val for pair in zip(idx, value) for val in pair]
        self.set_n(*args)

    def _display(self, status):
        return "---" if status is None else ("IN" if status else "OUT")
