from bliss.setup_globals import *
from bliss.common import session
from bliss.config import static
from bliss.shell.standard import umv, mv, umvr, mvr
from bliss.common.scans import ct, sct, ascan, loopscan
from bliss.shell import standard

config = static.get_config()

session =  config.get('lamino')
session.setup()
standard.flint()

mrsrot = session.env_dict['mrsrot']
print(mrsrot.position)

umv(mrsrot, 20)

print ("running ascan, please wait")
ascan(mrsrot, 20, 0, 10, 0.1, save=False)
print ("done")

shutter = session.env_dict['exp_shutter']
print (shutter.state)

keithley=session.env_dict['diode_cnt']
print (keithley.raw_read)

print ("counting")
ct(0.1)
print("done")

