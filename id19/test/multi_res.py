
from bliss.config.settings import ParametersWardrobe
from bliss.shell.standard import umv, umvr, mv, mvr


class multiRes:
    def __init__(self, name, config):
        self.__config = config
        #print (config)
        
        # Creats slits object to handle slit positions
        self.slits = slitsSnap(name, config)
        
        # Creats motors object to handle motor positions
        self.motors = motorsSnap(name, config)
        
        self.snapshots = ParametersWardrobe('default')
        self._init_default()
        
        
    def save(self, name, comment="None"):
        self.snapshots.switch(name)

        # save slits positions
        slits_dict = self.slits.save()
        # save motors positions
        motors_dict = self.motors.save()
        
        snapshot_dict = {}
        snapshot_dict["name"]    = name
        snapshot_dict["comment"] = comment
        snapshot_dict["slits"]   =  slits_dict
        snapshot_dict["motors"]  =  motors_dict
        self.snapshots.from_dict(snapshot_dict)
        
        
    def apply(self, name):
        s_list = self.snapshots.instances
        if name in s_list:
            self.snapshots.switch(name)
            
            # apply slits positions
            slits_dict = self.snapshots.slits
            self.slits.apply(slits_dict)
            
            # apply motors positions
            motors_dict = self.snapshots.motors
            
            # apply all motor positions
            #self.motors.apply(motors_dict)
            
            # apply only the positions of one motor group
            self.motors.apply(motors_dict, 'sample_position')
            self.motors.apply(motors_dict, 'detector_position')
            
        else:
            print("Unknown snapshot name!") 
        
        
    def remove(self, name):
        self.snapshots.remove(name)
        
        
    def purge(self):
        self.snapshots.purge()
        self._init_default()
        
    
    def _init_default(self):
        # init default structure
        self.snapshots.switch("default")
        
        # initialize slit defaults
        slits_dict = self.slits._init_default()
        
        # initialize motor defaults
        motors_dict = self.motors._init_default()
            
        self.snapshots.add("name", "None")
        self.snapshots.add("comment", "None")
        self.snapshots.add("slits", slits_dict)
        self.snapshots.add("motors", motors_dict)
        
        
    def show (self, name):
        # show the values of a snapshot
        
        s_list = self.snapshots.instances
        if name in s_list:
            self.snapshots.switch(name)
            print (self.snapshots.__info__())
        else:
            print("Unknown snapshot name!") 
        
        
    def __info__(self):
        status = "Available snapshots:\n"
        
        s_list = self.snapshots.instances
        for s in s_list:
            if s != "default":
                self.snapshots.switch(s)
                status += self.snapshots.name
                if  self.snapshots.comment == None:
                    status += " : "+ "None" +"\n"
                else:
                    status += " : "+ self.snapshots.comment +"\n"
                
        return status         
        
#
# Class to handle slit positions in a snapshot
#        
class slitsSnap:
    def __init__(self, name, config):
        
         # get all slits to snapshot
        self.slits_list= config.get("slits")
        
    def _init_default(self):
        # init slit default or reference valvues
        
        slits_dict = {}
        for s in self.slits_list:
            
            default=False
            s_config = self.slits_list[s]
            if "default" in s_config:
                default=True
            
            m_dict = {}
            for i in range(len(s_config['motors'])):
                if default == True:
                    m_dict[s_config['motors'][i].name] = s_config['default'][i]
                else:
                    m_dict[s_config['motors'][i].name] = 0.0
                
            slits_dict[s]=m_dict
        return slits_dict    
        
    def save(self):
        # Read positions from all specified slits
        
        slits_dict = {}
        for s in self.slits_list:
            s_config = self.slits_list[s]
            
            m_dict = {}
            for m in s_config['motors']:
                m_dict[m.name] = m.position
            slits_dict[s]=m_dict
        return slits_dict

    def apply(self, slits_dict):
        # Move all slits to the stored positions
        
        for s in slits_dict:
            print("Applying poitions to %s" % s)
            for mot in self.slits_list[s]['motors']:
                umv (mot, slits_dict[s][mot.name])
    
    
        
class motorsSnap:
    def __init__(self, name, config):
        
        # get all motors to snapshot
        self.motors_list= config.get('motors')

    def _init_default(self):
        # init motor default or reference valvues
        
        motors_dict = {}
        for mot in self.motors_list:
            mot_config = self.motors_list[mot]
            
            default=False
            if "default" in mot_config:
                default=True
            
            m_dict = {}
            for i in range(len(mot_config['motors'])):
                if default == True:
                    m_dict[mot_config['motors'][i].name] = mot_config['default'][i]
                else:
                    m_dict[mot_config['motors'][i].name] = 0.0
                
            motors_dict[mot]=m_dict
        return motors_dict

    def save(self):
        # Read positions from all specified motors
        
        motors_dict = {}
        for mot in self.motors_list:
            mot_config = self.motors_list[mot]
            
            m_dict = {}
            for m in mot_config['motors']:
                m_dict[m.name] = m.position
            motors_dict[mot]=m_dict
        return motors_dict
        
    def apply(self, motors_dict, mot_group=None):
        # Move all motors to the stored positions
        if mot_group == None:
            for mot_group in motors_dict:
                print("Applying poitions to %s" % mot_group)
                for mot in self.motors_list[mot_group]['motors']:
                    umv (mot, motors_dict[mot_group][mot.name])
        else:
            print("Applying poitions to %s" % mot_group)
            for mot in self.motors_list[mot_group]['motors']:
                umv (mot, motors_dict[mot_group][mot.name])
