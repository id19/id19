import numpy as np
import time
import datetime
import os
import gevent
import sys

from bliss.common.session import get_current_session
from bliss.config.settings import ParametersWardrobe
from bliss.common.standard import *
from bliss.common.axis import *
from bliss.shell.standard import *
from bliss.common.cleanup import *
from bliss.common.scans import *
from bliss.setup_globals import *
from bliss import setup_globals
from bliss.config.static import get_config

from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog,display

from PyTango import DeviceProxy

import tkinter as tk
from tkinter import ttk

class SampleChanger:
    def __init__(self, name, config):
        
        tango_server = config['tango_server'][0]
        self.samples_device = DeviceProxy(tango_server['samples'])
        self.robot_device = DeviceProxy(tango_server['robot'])
        
        if 'mrtomo' in get_current_session().object_names:
            self.tomo = 'mrtomo'
            mr_positions = config['positions'][1]['Mr']
            
            robot_positions = mr_positions['robot'].to_dict()
            experiment_positions = mr_positions['experiment'].to_dict()
            axis = config['axis'][1]['Mr']
                
        elif 'hrtomo' in get_current_session().object_names:
            self.tomo = 'hrtomo'
            hr_positions = config['positions'][0]['Hr']
            
            robot_positions = hr_positions['robot'].to_dict()
            experiment_positions = hr_positions['experiment'].to_dict()
            axis = config['axis'][0]['Hr']
            
        else:
            print('No sample change positions defined for this setup')
            return

        self.robot_positions = robot_positions
        self.experiment_positions = experiment_positions
        self.axis = axis
        
        self.sample_positions=list()
        for col in ['A','B','C']:
            for pos in range(1,19):
                if pos not in [7,8]:
                    self.sample_positions.append(col+str(pos))
        
        self.short_calib = False
        # list of scan sequence associated to each sample (fasttomo180, fasttomo360, zseries)
        self.tomo_sequence = []
    
    def __info__(self):
        
        info_str = "samplechanger info\n"
        info_str += f" samples_device = {self.samples_device}\n"
        info_str += f" robot_device = {self.robot_device}\n"
        info_str += f" tomo_station = {self.tomo}\n"
        info_str += f" robot_positions = {self.robot_positions}\n"
        info_str += f" experiment_positions = {self.experiment_positions}\n"
        info_str += f" axis = {self.axis}\n"
        info_str += f" active sample position = {self.robot_device.SamplePosition}\n"
        info_str += f" remote mode active = {self.robot_device.RemoteMode}\n"
        info_str += f" air pressure ok = {self.robot_device.AirPressureOK}\n"
        info_str += f" robot aligned = {self.robot_device.Aligned}\n"
        info_str += f" safety chain ok = {self.robot_device.SafetyChainOK}\n"
        info_str += f" sample loaded = {self.robot_device.SampleLoaded}\n"
        info_str += f" wago ok = {self.robot_device.WagoOK}\n"
        info_str += f" parking position = {self.robot_device.ParkingPosition}\n"
        info_str += f" robot status = {self.robot_device.State()}\n"
        return info_str
        
    #
    # Add a list of samples 
    #
    def add_samples(self):
        
        
        dlg_sample_size = UserCheckBox(label="Enter sample sizes", defval=False)
        
        dlg_message  = UserMsg(label="The sample positions A,B,C 7 and 8 do not exist on the sample changer shelf!")
        
        validator = Validator(self._valid_sample,'add')
        dlg_sample_position = UserInput(label="Sample position", defval='A1', validator=validator)
        dlg_sample_name = UserInput(label="Sample name", defval="Name")
        
        dlg_next = UserCheckBox(label="Next sample", defval=False)
        
        ret = True
        while ret != False:
        
            ret = BlissDialog( [[dlg_sample_size],[dlg_message],[dlg_sample_position],[dlg_sample_name],[dlg_next]], title='Add samples').show()

            if ret != False:
                sample = list()
                sample.append(ret[dlg_sample_position])
                sample.append(ret[dlg_sample_name])
                
                try:    
                    self.samples_device.AddSample(sample)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                
                if ret[dlg_sample_size]:
                    dlg_size_x = UserFloatInput(label="Sample size in X (mm)")
                    dlg_size_y = UserFloatInput(label="Sample size in Y (mm)")
                    dlg_size_z = UserFloatInput(label="Sample size in Z (mm)")
                    
                    ret2 = BlissDialog( [[dlg_size_x],[dlg_size_y],[dlg_size_z]], title='Add samples').show()
                    
                    if ret2 != False:
                        sample_size = list()
                        sample_size.append(ret2[dlg_size_x])
                        sample_size.append(ret2[dlg_size_y])
                        sample_size.append(ret2[dlg_size_z])
                        
                        try:
                            self.samples_device.ActivateSample(sample[0])
                        except Exception as exception:
                            self._tango_error(exception)
                            return
                        try:
                            self.samples_device.ActiveSampleSize = sample_size
                        except Exception as exception:
                            self._tango_error(exception)
                            return
            
                if not ret[dlg_next]:
                    break
                
                # this allows to suggest to user sample position following last sample position added 
                dlg_sample_position = UserInput(label="Sample position", defval=self.sample_positions[self.sample_positions.index(ret[dlg_sample_position])+1])
                dlg_sample_name = UserInput(label="Sample name", defval="Name")
            
        self.show_samples()
    
    #
    # Add a list of samples without user dialog 
    # Samples will have a common prefix name followed by an increasing number
    # Samples will have same aligned position for axis sz 
    #    
    def add_default_samples(self,nb_samples,prefix,align_sz):
        
        for nb in range(nb_samples):
            sample = list()
            sample_pos = self.sample_positions[nb]
            sample.append(sample_pos)
            if nb < 9:
                # name ex: 'prefixO1'
                sample_name = prefix+'0'+str(nb+1)
            else:
                # name ex: 'prefix10'
                sample_name = prefix+str(nb+1)
            sample.append(sample_name)
            
            try:            
                self.samples_device.AddSample(sample)
            except Exception as exception:
                self._tango_error(exception)
                return
            self.experiment_positions['sz'] = align_sz
    
    #
    # Delete a sample by specifying its name or position
    #
    def delete_samples(self):
        
        validator = Validator(self._valid_sample,'delete')
        dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)
        
        ret = BlissDialog([[dlg_sample]],title='Delete sample').show()
        
        if ret != False:
            sample = ret[dlg_sample]
            dlg_confirm = UserMsg(label=f"The sample {ret[dlg_sample]} is going to be deleted. Continue?")
            ret = BlissDialog([[dlg_confirm]],title='Delete sample').show()
            if ret != False:
                try:
                    self.samples_device.DeleteSample(sample)
                except Exception as exception:
                    self._tango_error(exception)
                    return

    #
    # Modify a sample by specifying its name or position 
    # All attributes (position, name, size and align position) can be modified
    #
    def edit_samples(self):
        
        validator = Validator(self._valid_sample,'edit')
        dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)
        
        ret = BlissDialog([[dlg_sample]],title='Edit sample').show()
        
        if ret != False:
            
            sample={}
            if ret[dlg_sample] in self.sample_positions:
                sample['position'] = ret[dlg_sample]
                try:
                    sample['name'] = self.samples_device.ActiveSampleName
                except Exception as exception:
                    self._tango_error(exception)
                    return
            else:
                sample['name'] = ret[dlg_sample]
                try:
                    lsamples = list(self.samples_device.Samples)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                # retrieve sample position from its name
                sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]
            
            # retrieve all sample attributes from its position 
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(sample['position'])
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['sizex'] = self.samples_device.ActiveSampleSize[0]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:    
                sample['sizey'] = self.samples_device.ActiveSampleSize[1]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['sizez'] = self.samples_device.ActiveSampleSize[2]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['alignsx'] = self.samples_device.ActiveSampleAlignPos[0]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:    
                sample['alignsy'] = self.samples_device.ActiveSampleAlignPos[1]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:    
                sample['alignsz'] = self.samples_device.ActiveSampleAlignPos[2]
            except Exception as exception:
                self._tango_error(exception)
                return
                
            lattr = [(0,'Position'),(1,'Name'),(2,'Size X'),(3,'Size Y'),(4,'Size Z'),(5,'Align SX'),(6,'Align SY'),(7,'Align SZ')]
            dlg_att_name = UserChoice(values=lattr,defval=0)
            ret = BlissDialog([[dlg_att_name]],title='Edit sample').show()
            if ret != False:
                attr = list()
                # retrieve existing key in 'sample' dictionary from 'lattr' list values
                key = lattr[ret[dlg_att_name]][1].replace(' ','')
                attr.append(key)
                # get actual value of attribute
                defval = sample[key.lower()]
                dlg_att_value = UserInput(label=f"{lattr[ret[dlg_att_name]][1]}",defval=defval)
                ret = BlissDialog([[dlg_att_value]],title='Edit sample').show()
                if ret != False:
                    attr.append(ret[dlg_att_value])
                    try:
                        # EditSample command is waiting for an array composed of attribute name and value
                        self.samples_device.EditSample(attr)
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    if attr[0] != 'Position':
                        try:
                            # if given attribute is position, sample activation is done is EditSample command
                            self.samples_device.ActivateSample(sample['position'])
                        except Exception as exception:
                            self._tango_error(exception)
                            return
                    try:
                        sample['position'] = self.samples_device.ActiveSamplePosition
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        sample['name'] = self.samples_device.ActiveSampleName
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        sample['sizex'] = self.samples_device.ActiveSampleSize[0]
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        sample['sizey'] = self.samples_device.ActiveSampleSize[1]
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        sample['sizez'] = self.samples_device.ActiveSampleSize[2]
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        sample['alignsx'] = self.samples_device.ActiveSampleAlignPos[0]
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        sample['alignsy'] = self.samples_device.ActiveSampleAlignPos[1]
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        sample['alignsz'] = self.samples_device.ActiveSampleAlignPos[2]
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    
                    # display all sample attributes to user
                    for attr in lattr:
                        attr_name = attr[1].replace(' ','')
                        print(f"{attr_name}: {sample[attr_name.lower()]}")
                   
                    print("\nYour change is effective")
    
    #
    # Read all dimensions of a sample and return the values
    #                 
    def read_sample_size(self):
        
        validator = Validator(self._valid_sample,'read')
        dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)
        
        ret = BlissDialog([[dlg_sample]],title='Read sample size').show()
        
        if ret != False:
            
            sample={}
            if ret[dlg_sample] in self.sample_positions:
                sample['position'] = ret[dlg_sample]
            else:
                try:
                    lsamples = list(self.samples_device.Samples)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                # retrieve sample position from its name
                sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]
            
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(sample['position'])
            except Exception as exception:
                self._tango_error(exception)
                return
            
            try:
                sample['sizex'] = self.samples_device.ActiveSampleSize[0]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['sizey'] = self.samples_device.ActiveSampleSize[1]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['sizez'] = self.samples_device.ActiveSampleSize[2]
            except Exception as exception:
                self._tango_error(exception)
                return
            
            # display all sample dimensions to user
            print('\n')
            for dim in ['X','Y','Z']:
                key = 'size'+dim.lower() 
                print(f"Sample size in {dim} (mm): {sample[key]}")
            print('\n')
    
    #
    # Define sample size in X,Y and Z dimensions
    #
    def write_sample_size(self):
        
        validator = Validator(self._valid_sample,'write')
        dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)
        
        ret = BlissDialog([[dlg_sample]],title='Write sample size').show()
        
        if ret != False:
            
            sample={}
            if ret[dlg_sample] in self.sample_positions:
                sample['position'] = ret[dlg_sample]
            else:
                try:
                    lsamples = list(self.samples_device.Samples)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                # retrieve sample position from its name
                sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]
            
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(sample['position'])
            except Exception as exception:
                self._tango_error(exception)
                return
                
            dlg_sizex = UserFloatInput(label="Sample size in X (mm)", defval=0.0)
            dlg_sizey = UserFloatInput(label="Sample size in Y (mm)", defval=0.0)
            dlg_sizez = UserFloatInput(label="Sample size in Z (mm)", defval=0.0)
        
            ret = BlissDialog([[dlg_sizex],[dlg_sizey],[dlg_sizez]],title='Write sample size').show()
            
            if ret != False:
                
                sample_size=[ret[dlg_sizex],ret[dlg_sizey],ret[dlg_sizez]]
                try:
                    self.samples_device.ActiveSampleSize = sample_size
                except Exception as exception:
                    self._tango_error(exception)
                    return
    
    #
    # Read all alignment axis positions of a sample and return the values if sample is aligned
    #               
    def read_align_pos(self):
        
        validator = Validator(self._valid_sample,'read')
        dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)
        
        ret = BlissDialog([[dlg_sample]],title='Read sample alignment position').show()
        
        if ret != False:
            
            sample={}
            if ret[dlg_sample] in self.sample_positions:
                sample['position'] = ret[dlg_sample]
            else:
                try:
                    lsamples = list(self.samples_device.Samples)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                # retrieve sample position from its name
                sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]
            
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(sample['position'])
            except Exception as exception:
                self._tango_error(exception)
                return
            
            try:
                sample['alignsx'] = self.samples_device.ActiveSampleAlignPos[0]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['alignsy'] = self.samples_device.ActiveSampleAlignPos[1]
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['alignsz'] = self.samples_device.ActiveSampleAlignPos[2]
            except Exception as exception:
                self._tango_error(exception)
                return
            
            try:
                # check if sample is aligned
                align = self.samples_device.ActiveAligned
            except Exception as exception:
                self._tango_error(exception)
                return
            if align:
                print('\n')
                for dim in ['X','Y','Z']:
                    key = 'aligns'+dim.lower() 
                    print(f"Alignment position in {dim} (mm): {sample[key]}")
                print('\n')
            else:
                print(f"The sample {sample['position']} is not aligned yet!\n")
    
    #
    # Define manually sx, sy, sz positions when sample is aligned = sample alignment position
    #
    def write_align_pos(self):
        
        validator = Validator(self._valid_sample,'write')
        dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)
        
        ret = BlissDialog([[dlg_sample]],title='Write sample alignment position').show()
        
        if ret != False:
            
            sample={}
            if ret[dlg_sample] in self.sample_positions:
                sample['position'] = ret[dlg_sample]
            else:
                try:
                    lsamples = list(self.samples_device.Samples)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                # retrieve sample position from its name
                sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]
            
            try:    
                # select a sample from its position
                self.samples_device.ActivateSample(sample['position'])
            except Exception as exception:
                self._tango_error(exception)
                return
                
            dlg_alignsx = UserFloatInput(label="Alignment position in X (mm)", defval=0.0)
            dlg_alignsy = UserFloatInput(label="Alignment position in Y (mm)", defval=0.0)
            dlg_alignsz = UserFloatInput(label="Alignment position in Z (mm)", defval=0.0)
        
            ret = BlissDialog([[dlg_alignsx],[dlg_alignsy],[dlg_alignsz]],title='Write sample alignment position').show()
            
            if ret != False:
                
                align_pos = [ret[dlg_alignsx],ret[dlg_alignsy],ret[dlg_alignsz]]
                try:
                    self.samples_device.ActiveSampleAlignPos = align_pos
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    # sample is now aligned
                    self.samples_device.ActiveAligned = True
                except Exception as exception:
                    self._tango_error(exception)
                    return
    
    #
    # Define actual sx, sy, sz positions as sample alignment position
    #            
    def store_sample_align_pos(self):
        
        # get the position of the loaded sample
        try:
            sample_pos = self.robot_device.SamplePosition
        except Exception as exception:
            self._tango_error(exception)
            return
            
        if sample_pos != "None":
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(sample_pos)
            except Exception as exception:
                self._tango_error(exception)
                return
            
            try:    
                sample_name = self.samples_device.ActiveSampleName
            except Exception as exception:
                self._tango_error(exception)
                return
            
            dlg_message = UserMsg(label=f"Sample {sample_name} is now aligned")
            dlg_confirm = UserMsg(label=f"Sample alignment positions are going to be stored. Continue?")
            ret = BlissDialog([[dlg_message],[dlg_confirm]],title='Store sample alignment positions').show()
    
            if ret != False:
                align_pos = [self.axis.get('sx').position,self.axis.get('sy').position,self.axis.get('sz').position]
                try:
                    self.samples_device.ActiveSampleAlignPos = align_pos
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    # sample is now aligned
                    self.samples_device.ActiveAligned = True
                except Exception as exception:
                    self._tango_error(exception)
                    return
                  
                print(f"\nStored alignment positions: {align_pos[0]}, {align_pos[1]}, {align_pos[2]}\n")
        else:
            print("No sample loaded by the sample changer!")
    
    def store_experiment_positions(self):
        
        self.experiment_positions['xc'] = self.axis.get('xc').position
        
        self.experiment_positions['yrot'] = self.axis.get('yrot').position
        
        string = f"\nStored experiment positions: xc: {self.experiment_positions['xc']}, yrot: {self.experiment_positions['yrot']}"
            
        if self.tomo == 'hrtomo':
            self.experiment_positions['z0'] = self.axis.get('z0').position
            
            string += f", z0: {self.experiment_positions['z0']}"
        
        print(string+"\n")
    #
    # Show all defined samples
    #
    def show_samples(self,wait=True):
        
        try:
            samples = self.samples_device.Samples
        except Exception as exception:
            self._tango_error(exception)
            return
        sample = {}
        
        # window creation
        window = tk.Tk()
        # define window title
        window.title('Available samples')
        # table creation
        table = ttk.Treeview(window)
        # define table columns 
        table["columns"]=("name","sizex","sizey","sizez","alignsx","alignsy","alignsz")
        table.column("#0", width=150, minwidth=150, stretch=tk.NO)
        table.column("name", width=150, minwidth=150, stretch=tk.NO)
        table.column("sizex", width=150, minwidth=150, stretch=tk.NO)
        table.column("sizey", width=150, minwidth=150, stretch=tk.NO)
        table.column("sizez", width=150, minwidth=150, stretch=tk.NO)
        table.column("alignsx", width=150, minwidth=150, stretch=tk.NO)
        table.column("alignsy", width=150, minwidth=150, stretch=tk.NO)
        table.column("alignsz", width=150, minwidth=150, stretch=tk.NO)
        
        # define columns name
        table.heading("#0",text="Position",anchor=tk.W)
        table.heading("name", text="Name",anchor=tk.W)
        table.heading("sizex", text="Size X",anchor=tk.W)
        table.heading("sizey", text="Size Y",anchor=tk.W)
        table.heading("sizez", text="Size Z",anchor=tk.W)
        table.heading("alignsx", text="Align SX",anchor=tk.W)
        table.heading("alignsy", text="Align SY",anchor=tk.W)
        table.heading("alignsz", text="Align SZ",anchor=tk.W)
         
        for i in range(0,len(samples),2):
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(samples[i])
            except Exception as exception:
                self._tango_error(exception)
                return
            sample['position'] = samples[i]
            try:
                sample['name'] = self.samples_device.ActiveSampleName
            except Exception as exception:
                self._tango_error(exception)
                return
            try:    
                sample['size'] = self.samples_device.ActiveSampleSize
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['alignpos'] = self.samples_device.ActiveSampleAlignPos
            except Exception as exception:
                self._tango_error(exception)
                return
            # add each sample in table
            table.insert("", i+1, text=str(sample['position']), values=(str(sample['name']),str(sample['size'][0]),str(sample['size'][1]),str(sample['size'][2]),str(sample['alignpos'][0]),str(sample['alignpos'][1]),str(sample['alignpos'][2])))
        
        # insert table inside window
        table.pack(side=tk.TOP,fill=tk.X)
        # display window
        if wait:
            # blocking window
            window.mainloop()
        else:
            # non blocking window
            window.update()
            # return window object
            return window
    
    #
    # Delete all available samples
    #    
    def reset_samples(self):
        
        dlg_confirm = UserMsg(label=f"All defined samples are going to be deleted. Continue?")
        ret = BlissDialog([[dlg_confirm]],title='Reset samples').show()
        
        if ret != False:
            try:
                self.samples_device.Reset()
            except Exception as exception:
                self._tango_error(exception)
                return
            print("All defined samples are deleted!")
    
    #
    # Define scan sequence (fasttomo180, fasttomo360, zseries) associated to each sample
    #    
    def build_scan_sequence(self):
        
        scan_name = "fasttomo180"
        sz_step = 1
        nr_scans = 1
        try:
            lsamples = self.samples_device.Samples
        except Exception as exception:
            self._tango_error(exception)
            return
        
        # get window gui object
        sample_wind = self.show_samples(wait=False)
        
        validator = Validator(self._valid_sample,'find')
        
        ret = True
        next_sample = lsamples[0]
        while ret != False:

            dlg_sample_position = UserInput(label="Sample position to be scanned", defval=next_sample, validator=validator)
            
            ret = BlissDialog( [[dlg_sample_position]], title='Build scan sequence').show()
            
            if ret != False:
                sample = {}
                sample['position'] = ret[dlg_sample_position]
                sample_name = lsamples[lsamples.index(ret[dlg_sample_position])+1]
                sample['name'] = sample_name
                dlg_sample_name = UserMsg(label=f"Sample name is: {sample_name}")
                scan_sequence = [(0,"fasttomo180"),(1,"fasttomo360"),(2,"zseries")]
                dlg_scan_sequence = UserChoice(label="Scan sequence", values=scan_sequence, defval=0)
                
                ret = BlissDialog( [[dlg_sample_name],[dlg_scan_sequence]], title='Build scan sequence').show()
            
                if ret != False:
                    scan_name = scan_sequence[ret[dlg_scan_sequence]][1]
                    
                    sample['scan'] = [scan_name]
                    dlg_message = UserMsg(label=f"position: {sample['position']}\nname: {sample['name']}\nscan name: {scan_name}")
                    
                    if scan_name == 'zseries':
                        dlg_sz_step = UserFloatInput(label="Sz step",defval=sz_step)
                        dlg_nr_scans = UserIntInput(label="Total number of scans",defval=nr_scans)
                        
                        ret = BlissDialog( [[dlg_sz_step],[dlg_nr_scans]], title='Build scan sequence').show()
            
                        if ret != False:
                            sz_step = ret[dlg_sz_step]
                            nr_scans = ret[dlg_nr_scans]
                            
                            sample['scan'].append(sz_step)
                            sample['scan'].append(nr_scans)
                            
                            dlg_message = UserMsg(label=f"position: {sample['position']}\nname: {sample['name']}\nscan name: {scan_name}\nsz step: {sz_step}\nnb scans: {nr_scans}")
                            
    
                dlg_next = UserCheckBox(label="Next sample", defval=True)
                    
                ret = BlissDialog( [[dlg_message],[dlg_next]],title='Build scan sequence').show()
                 
                if ret != False:
                    self.tomo_sequence.append(sample)
                    
                    if not ret[dlg_next]:
                        break
                    else:
                        next_sample = lsamples[lsamples.index(sample['name'])+1]
                
                    if len(self.tomo_sequence) == len(lsamples)/2:
                        break
        
        # close window gui object    
        sample_wind.destroy()
        
        if ret != False:
            self.show_scan_sequence()
        

    
    #
    # Execute scan sequence
    #
    def execute_scan_sequence(self):
        tomo = get_config().get(self.tomo)
        
        start_scan = UserYesNo(label="Do you want to run the scan sequence now?",defval=True)
        ret = display(start_scan, title='Execute scan sequence')
    
        if ret != False:
            
            for sample in range(len(self.tomo_sequence)):
                
                self.load_sample(self.tomo_sequence[sample]['position'])
                
                setup_globals.newsample(self.tomo_sequence[sample]['name'])
                
                print("Beam shutter closing")
                tomo.shutter.beam_shutter_close()
                
                if self.tomo_sequence[sample]['scan'][0] == 'fasttomo180':
                    
                    tomo.half_turn_scan()
                    
                elif self.tomo_sequence[sample]['scan'][0] == 'fasttomo360':
                    
                    tomo.full_turn_scan()
                
                elif self.tomo_sequence[sample]['scan'][0] == 'zseries':
                    
                    tomo.zseries(self.tomo_sequence[sample]['scan'][1],self.tomo_sequence[sample]['scan'][2])
                
                self.unload_sample()
    
    #
    # Show scan sequence
    #
    def show_scan_sequence(self):
        
        if len(self.tomo_sequence) > 0:
            # window creation
            window = tk.Tk()
            # define window title
            window.title('Configured scan sequences')
            # table creation
            table = ttk.Treeview(window)
            # define table columns 
            table["columns"]=["position","name","scan","zstep","nbscans"]
            table.column("#0", width=150, minwidth=150, stretch=tk.NO)
            table.column("position", width=150, minwidth=150, stretch=tk.NO)
            table.column("name", width=150, minwidth=150, stretch=tk.NO)
            table.column("scan", width=150, minwidth=150, stretch=tk.NO)
            table.column("zstep", width=150, minwidth=150, stretch=tk.NO)
            table.column("nbscans", width=150, minwidth=150, stretch=tk.NO)
            
            # define columns name
            table.heading("#0",text="Index",anchor=tk.W)
            table.heading("position", text="Position",anchor=tk.W)
            table.heading("name", text="Name",anchor=tk.W)
            table.heading("scan", text="Sequence",anchor=tk.W)
            table.heading("zstep", text="Z Step",anchor=tk.W)
            table.heading("nbscans", text="Number Of Scans",anchor=tk.W)
            
            for sample in range(len(self.tomo_sequence)):
                # add each sample in table
                values = [str(self.tomo_sequence[sample]['position']),str(self.tomo_sequence[sample]['name'])]
                for val in self.tomo_sequence[sample]['scan']:
                    values.append(str(val))
                table.insert("", sample+1, text=str(sample+1), values=values)
        
            # insert table inside window
            table.pack(side=tk.TOP,fill=tk.X)
            # display window
            window.mainloop()
        else:
            print('\nNo scan sequence has been configured for now\n')
    
    #
    # Sample changer robot calls
    #    
    def load_sample(self, sample):
        
        try:
            try:
                lsamples = list(self.samples_device.Samples)
            except Exception as exception:
                self._tango_error(exception)
                return
            # test that sample exits and is added 
            if sample in lsamples:
                if sample not in self.sample_positions :
                    # get sample position by sample name
                    sample = lsamples[lsamples.index(sample)-1]
                    
                try:    
                    self.samples_device.ActivateSample(sample)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                    
                # read alignment positions when aligned
                try:
                    align = self.samples_device.ActiveAligned
                except Exception as exception:
                    self._tango_error(exception)
                    return
                        
                if align:
                    try:
                        align_pos = self.samples_device.ActiveSampleAlignPos
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                else:
                    # if not aligned, go to the default experiment positions
                    align_pos = [0,0,self.experiment_positions['sz']]
                    
                print("\nMove to sample change position")
                self.change_position()
                try:
                    self.robot_device.LoadSample(sample)
                except Exception as exception:
                    self._tango_error(exception)
                    return
                
                print(f"\nLoading sample {sample}")
                print("Robot moving...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                while robot_state == 6:
                    time.sleep(3)
                    print("Robot moving...",end="\r")
                    try:
                        robot_state = self.robot_device.State()
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                
                time.sleep(1)
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                if robot_state == 0:
                    print("\nMove to experiment position")
                    self.experiment_position(align_pos[0],align_pos[1],align_pos[2])
                    print("\nSample loading done!")
                    
                    tomo = get_config().get(self.tomo)
                    
                    if self.robot_device.RemoteMode: 
                        print("Beam shutter opening")
                        tomo.shutter.beam_shutter_open()
                else:
                    print("\nSample loading did not finish as expected!")
                    try:
                        robot_status = self.robot_device.Status()
                    except Exception as exception:
                        self._tango_error(exception)
                        return    
                    print(f"\nThe sample changer status is:{robot_status}")
            else:
                print('This sample does not exist') 
                    
        except KeyboardInterrupt:
            print('\n\nSample loading aborted')
            self.abort()
            raise
    
    def unload_sample(self):
        
        try:
            tomo = get_config().get(self.tomo)
            
            if self.robot_device.RemoteMode: 
                print("Beam shutter closing")
                tomo.shutter.beam_shutter_close()
        
            print("\nMove to sample change position")
            self.change_position()
        
            # unload activated sample loaded on tomograph
            sample = self.samples_device.ActiveSamplePosition
            print(f"\nUnloading sample {sample}")
            print("Robot moving...",end="\r")
            try:
                self.robot_device.UnloadSample()
            except Exception as exception:
                self._tango_error(exception)
                return
            
            try:
                robot_state = self.robot_device.State()
            except Exception as exception:
                self._tango_error(exception)
                return
            while robot_state == 6:
                time.sleep(3)
                print("Robot moving...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                    
            time.sleep(1)
            try:
                robot_state = self.robot_device.State()
            except Exception as exception:
                self._tango_error(exception)
                return
            if robot_state == 0:
                print("\nSample unloading done!")
            else:
                print("\nSample unloading did not finish as expected!")
                try:
                    robot_status = self.robot_device.Status()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                print(f"\nThe sample changer status is:{robot_status}")
                
        except KeyboardInterrupt:
            print('\n\nSample unloading aborted')
            self.abort()
            raise
                
    def align(self):
        
        print("Move to alignment position")
        self.change_position()
    
        print("\nYou need enter the hutch and survey the procedure with the local control!")
        print("For the alignment the sample changer has to be in manual mode!")
    
        confirm = UserYesNo(label="\nIs the sample changer in manual mode?",defval=True)
        ret = display(confirm, title='Align sample changer')
    
        if ret != False:
            print("\nStarting alignment procedure\n")
            try:
                self.robot_device.Align()
            except Exception as exception:
                self._tango_error(exception)
                return
            time.sleep(2)
            print("Robot is ready to be moved...",end="\r")
            try:
                robot_state = self.robot_device.State()
            except Exception as exception:
                self._tango_error(exception)
                return
            # test to 6 == robot is moving, if not return error!
            while robot_state == 6:
                time.sleep(3)
                print("Robot is ready to be moved...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                
            time.sleep(1)
            try:
                robot_state = self.robot_device.State()   
            except Exception as exception:
                self._tango_error(exception)
                return
            if robot_state == 0:
                try:
                    is_aligned = self.robot_device.Aligned
                except Exception as exception:
                    self._tango_error(exception)
                    return
                if is_aligned == 1:
                    print("\n\nThe sample changer is aligned!")
                else:
                    print("\n\nThe alignment procedure ended without error, but the alignment failed!")
                    print("Please check the sample changer!")
                
            else:
                print("\nAlignment did not finish as expected!")
                try:
                    robot_status = self.robot_device.Status()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                print(f"\nThe sample changer status is:{robot_status}")

    def abort(self):
        
        print("\nStopping all robot movements!\n")
        try:
            self.robot_device.Abort()
        except Exception as exception:
            self._tango_error(exception)
            return
    
    def parking_position(self):
        
        print("\nYou need enter the hutch and survey the procedure with the local control!")
        print("For the recovery procedure, the sample changer has to be in manual mode!")
        
        confirm = UserYesNo(label="\nIs the sample changer in manual mode",defval=True)
        ret = display(confirm, title='Parking position')
        
        if ret != False:
            print("\nStarting recovery procedure\n")
            try:
                self.robot_device.ForceParkingPosition()
            except Exception as exception:
                self._tango_error(exception)
                return
            
            time.sleep(2)
            print("Robot moving...",end="\r")
            try:
                robot_state = self.robot_device.State()       
            except Exception as exception:
                self._tango_error(exception)
                return
            # test to 6 == robot is moving, if not return error!
            while robot_state == 6:
                time.sleep(3)
                print("Robot moving...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
            
            time.sleep(1)
            try:
                robot_state = self.robot_device.State() 
            except Exception as exception:
                self._tango_error(exception)
                return  
            if robot_state == 8:
                try:
                    is_parking = self.robot_device.ParkingPosition
                except Exception as exception:
                    self._tango_error(exception)
                    return
                if is_parking == 1:
                    print("\n\nReset the fault state")
                    try:
                        self.robot_device.Reset()
                    except Exception as exception:
                        self._tango_error(exception)
                        return   
                    self.manual_unload()
                else:
                    print("\n\nThe recovery procedure ended without error, but the parking position was not reached")
                    print("Please check the sample changer!")
            else:
                print("\nRecover did not finish as expected!")
                try:
                    robot_status = self.robot_device.Status()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                print(f"\nThe sample changer status is:{robot_status}")
                
    #
    # unset sample loaded 
    #                
    def manual_unload(self):

        confirm = UserYesNo(label="Please remove the sample manually\n Is the sample removed now?",defval=True)
        ret = display(confirm, title='Manual unload')
    
        if ret != False:
            try:
                self.robot_device.ManualUnload()
            except Exception as exception:
                self._tango_error(exception)
                return
                
    def open_gripper(self):
        
        print("\nOpen the gripper\n")
        try:
            self.robot_device.OpenGripper()
        except Exception as exception:
            self._tango_error(exception)
            return
        
    def close_gripper(self):
        
        print("\nClose the gripper\n")
        try:
            self.robot_device.CloseGripper()
        except Exception as exception:
            self._tango_error(exception)
            return
    #
    # Robot positioning
    #
        
    def change_position(self):
        
        # Move the detector away
        det_pos = self.axis.get('xc').position
        print(f"Current detector position: {det_pos}")
        if self.robot_positions['xc'] > det_pos:
            det_pos = self.robot_positions['xc']
            print("\nMove the detector away")
            print(f"Move detector to: {det_pos}")
            umv(self.axis.get('xc'),det_pos)
        
        # umvd equivalent, must be change when bliss will be updated    
        dial_pos1 = self.robot_positions['yrot']
        pos1 = self.axis.get('yrot').sign*dial_pos1 + self.axis.get('yrot').offset
        umv(self.axis.get('yrot'),pos1)
        
        if 'z0' in self.axis:
            pos = self.robot_positions['z0']
            print(f"\nMove z0 to: {pos}")
            umv(self.axis.get('z0'),pos)
            
        # move the dial positions
        # umvd equivalent, must be change when bliss will be updated
        dial_pos1 = self.robot_positions['srot']
        pos1 = self.axis.get('srot').sign*dial_pos1 + self.axis.get('srot').offset
        print(f"\nMove srot to {pos1}")
        dial_pos2 = self.robot_positions['sx']
        pos2 = self.axis.get('sx').sign*dial_pos2 + self.axis.get('sx').offset
        print(f"\nMove sx to {pos2}")
        dial_pos3 = self.robot_positions['sy']
        pos3 = self.axis.get('sy').sign*dial_pos3 + self.axis.get('sy').offset
        print(f"\nMove sy to {pos3}")
        dial_pos4 = self.robot_positions['sz']
        pos4 = self.axis.get('sz').sign*dial_pos4 + self.axis.get('sz').offset
        print(f"\nMove sz to {pos4}")
        umv(self.axis.get('srot'),pos1,self.axis.get('sx'),pos2,self.axis.get('sy'),pos3,self.axis.get('sz'),pos4)

        # still some timeouts on the robot that are longer than 3 seconds!
        try:
            self.robot_device.set_timeout_millis(20000)
        except Exception as exception:
            self._tango_error(exception)
            return
        
    def experiment_position(self,sx_pos,sy_pos,sz_pos):
        
        print(f"\nMove sz to: {sz_pos}")   
        umv(self.axis.get('sx'),sx_pos,self.axis.get('sy'),sy_pos,self.axis.get('sz'),sz_pos)
        
        if 'z0' in self.axis:
            pos = self.experiment_positions['z0']
            print(f"\nMove z0 to: {pos}")
            umv(self.axis.get('z0'),pos)
        
        pos = self.experiment_positions['yrot']
        print(f"\nMove yrot to: {pos}")
        umv(self.axis.get('yrot'),pos)
        
        pos = self.experiment_positions['xc']
        print("\nMove the detector closer")
        print(f"Move detector to: {pos}")
        umv(self.axis.get('xc'),pos)

         
    def _tango_error(self,exception):
        print("Failed to execute: %s\n" % exception.args[0].origin)
        print(exception.args[0].desc)
    
    #
    # verify if a given position is valid (exists) or is not already used
    #     
    def _valid_sample(self,str_input,action):
        try:
            lsamples = list(self.samples_device.Samples)
        except Exception as exception:
            self._tango_error(exception)
            return
        if action == 'add':
            if str_input in lsamples:
                raise ValueError(f"A sample is already defined at position {str_input}")
            elif str_input not in self.sample_positions:
                raise ValueError("This position does not exist, Please enter a new one")
        if action != 'add' and str_input not in lsamples:
            raise ValueError(f"This sample does not exist, Please enter a new one")

# calculate execution time of a function           
def bench(func_name,*args,**kwargs):
    start_time = time.time()
    func_name(*args,**kwargs)
    tot_time = (time.time()-start_time) / 60.0 if (time.time()-start_time) > 60.0 else (time.time()-start_time)
    time_unit = 'min' if (time.time()-start_time) > 60.0 else 'sec'
    print(f'Took: {round(tot_time,2)} {time_unit}')

# load and unload all defined samples
def load_unload_samples():
    
    sample_changer = get_config().get('samplechanger')
    
    lsamples = sample_changer.samples_device.Samples
    
    for i in range(0,len(lsamples),2):
        if lsamples[i] != 'A18':
            print("-------------------------------")
            print(datetime.datetime.now())
            print("-------------------------------")
            print(f'Change number {lsamples[i][1:]}\n')
            print(f"Bench sample_changer.load_sample('{lsamples[i]}')\n")
            bench(sample_changer.load_sample,lsamples[i])
            time.sleep(5)
            bench(sample_changer.unload_sample)
            time.sleep(5)

    print("J ai fini")
    
# load, scan and unload all defined samples in tomo sequence list   
def scan_all_samples():
    
    sample_changer = get_config().get('samplechanger')
    tomo = get_config().get(self.tomo)
    
    if len(sample_changer.tomo_sequence) > 0: 
        
        for sample in sample_changer.tomo_sequence:
            
            try:
                print("-------------------------------")
                print(datetime.datetime.now())
                print("-------------------------------")
                print(f"Scan sample number {sample_changer.tomo_sequence[sample]['position'][1:]}\n")
                print(f"Bench sample_changer.load_sample('{sample_changer.tomo_sequence[sample]['position']}')\n")        
                t0 = time.time()
                try:
                    bench(sample_changer.load_sample,sample_changer.tomo_sequence[sample]['position'])
                except Exception as exception:
                    self._tango_error(exception)
                
                setup_globals.newsample(sample_changer.tomo_sequence[sample]['name'])
                
                time.sleep(5)
                        
                if sample_changer.tomo_sequence[sample]['scan'][0] == 'fasttomo180':
                            
                    tomo.half_turn_scan()
                            
                elif sample_changer.tomo_sequence[sample]['scan'][0] == 'fasttomo360':
                            
                    tomo.full_turn_scan()
                        
                elif sample_changer.tomo_sequence[sample]['scan'][0] == 'zseries':
                            
                    tomo.zseries(sample_changer.tomo_sequence[sample]['scan'][1],sample_changer.tomo_sequence[sample]['scan'][2])
                
                time.sleep(5)
                
                try:        
                    bench(sample_changer.unload_sample)
                except Exception as exception:
                    self._tango_error(exception)
                
                time.sleep(5)
                
                run_time = time.time() - t0 - 15
                if run_time >= 60:
                    run_time /= 60
                    unit = 'min'
                else:
                    unit = 'sec'
                print(f"Total time for sample {sample_changer.tomo_sequence[sample]['position']} took {run_time} {unit}")
            except Exception:
                raise   
    else:
        print('\nNo scan sequence has been configured for now\n')
        
# load, take one image at 0 and 90 degrees and unload all samples    
def radio_all_samples(expo_time_tomo=0.5,expo_time_vis=0.5):
    
    sample_changer = get_config().get('samplechanger')
    
    tomo = get_config().get(self.tomo)
    
    tomo_ccd = tomo.tomo_ccd
    
    os.makedirs(tomo.saving.get_path(), exist_ok=True)
    
    tomo.saving.apply_scan_saving()
    # clear detector saving index format
    for det_name in default_mg.enabled:
        det = get_config().get(det_name)
        det.proxy.saving_index_format=''
    
    lsamples = sample_changer.samples_device.Samples
    
    for i in range(0,len(lsamples),2):
        try:
            if lsamples[i] != 'A18':
                print("-------------------------------")
                print(datetime.datetime.now())
                print("-------------------------------")
                print(f'Change number {lsamples[i][1:]}\n')
                t0 = time.time()
                try:
                    # load sample
                    bench(sample_changer.load_sample,lsamples[i])
                except Exception as exception:
                    self._tango_error(exception)
                time.sleep(5)
                tomo.saving.scan_saving.images_path_template = '{tomo_scan_name}_'+lsamples[i]
                print('\nImages saving path: '+tomo.saving.get_path()+'/'+tomo.saving.scan_saving.tomo_scan_name+'_'+lsamples[i])
                print('Data saving path: '+tomo.saving.get_path())
                print('\nMoving rotation to 0')
                umv(tomo.rotation_axis,0)
                print(f'\nTaking image with an exposure time of {tomo.parameters.exposure_time} sec')
                # take image at 0 degree with tomo detector
                tomo.saving.scan_saving.images_prefix = lsamples[i]+'_tomo'+'%04d'%tomo.rotation_axis.position
                det_tomo = get_config().get(default_mg.enabled[0])
                bench(ct,expo_time_tomo,det_tomo,name=lsamples[i],save=True,save_images=True)
                # take image at 0 degree with visible detector
                tomo.saving.scan_saving.images_prefix = lsamples[i]+'_vis'+'%04d'%tomo.rotation_axis.position
                det_vis = get_config().get(default_mg.enabled[1])
                bench(ct,expo_time_vis,det_vis,name=lsamples[i],save=True,save_images=True)
                print('\nMoving rotation to 90')
                umv(tomo.rotation_axis,90)
                print(f'\nTaking image with an exposure time of {tomo.parameters.exposure_time} sec')
                # take image at 90 degrees with tomo detector
                tomo.saving.scan_saving.images_prefix = lsamples[i]+'_tomo'+'%04d'%tomo.rotation_axis.position
                det_tomo = get_config().get(default_mg.enabled[0])
                bench(ct,expo_time_tomo,det_tomo,name=lsamples[i],save=True,save_images=True)
                # take image at 90 degrees with visible detector
                tomo.saving.scan_saving.images_prefix = lsamples[i]+'_vis'+'%04d'%tomo.rotation_axis.position
                det_vis = get_config().get(default_mg.enabled[1])
                bench(ct,expo_time_vis,det_vis,name=lsamples[i],save=True,save_images=True)
                time.sleep(5)
                try:
                    # unload sample
                    bench(sample_changer.unload_sample)
                except Exception as exception:
                    self._tango_error(exception)
                time.sleep(5)
                run_time = time.time() - t0 - 15
                if run_time >= 60:
                    run_time /= 60
                    unit = 'min'
                else:
                    unit = 'sec'
                print(f"Total time for sample {lsamples[i]}: {round(run_time,2)} {unit}")
        except Exception:
            raise 
    
    # reset detector saving index format to previous format
    for det_name in default_mg.enabled:
        det = get_config().get(det_name)
        det.proxy.saving_index_format='%04d'
    
# return robot status (ON,FAULT,DISABLE,MOVING,UNKNOWN)
def robot_status():
    
    sample_changer = get_config().get('samplechanger')
    
    print(sample_changer.robot_device.Status())
