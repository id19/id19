# -*- coding: utf-8 -*-
#
# This file is part of the HadesPress project
#
# Copyright (C): 2019
#                European Synchrotron Radiation Facility
#                BP 220, Grenoble 38043
#                France
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" 

"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(HadesPress.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  HadesPress.additionnal_import

__all__ = ["HadesPress", "main"]


class HadesPress(Device):
    """
    """
    # PROTECTED REGION ID(HadesPress.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  HadesPress.class_variable

    # ----------
    # Attributes
    # ----------

    Pressure = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
    )

    PressLock = attribute(
        dtype='DevBoolean',
    )

    ScanLock = attribute(
        dtype='DevBoolean',
        access=AttrWriteType.READ_WRITE,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the HadesPress."""
        Device.init_device(self)
        # PROTECTED REGION ID(HadesPress.init_device) ENABLED START #
        # PROTECTED REGION END #    //  HadesPress.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(HadesPress.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  HadesPress.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(HadesPress.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  HadesPress.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_Pressure(self):
        # PROTECTED REGION ID(HadesPress.Pressure_read) ENABLED START #
        """Return the Pressure attribute."""
        return self.__pressure
        # PROTECTED REGION END #    //  HadesPress.Pressure_read

    def write_Pressure(self, value):
        # PROTECTED REGION ID(HadesPress.Pressure_write) ENABLED START #
        """Set the Pressure attribute."""
        pass
        # PROTECTED REGION END #    //  HadesPress.Pressure_write

    def read_PressLock(self):
        # PROTECTED REGION ID(HadesPress.PressLock_read) ENABLED START #
        """Return the PressLock attribute."""
        return self.__press_lock
        # PROTECTED REGION END #    //  HadesPress.PressLock_read

    def read_ScanLock(self):
        # PROTECTED REGION ID(HadesPress.ScanLock_read) ENABLED START #
        """Return the ScanLock attribute."""
        return self.__scan_lock
        # PROTECTED REGION END #    //  HadesPress.ScanLock_read

    def write_ScanLock(self, value):
        # PROTECTED REGION ID(HadesPress.ScanLock_write) ENABLED START #
        """Set the ScanLock attribute."""
        pass
        # PROTECTED REGION END #    //  HadesPress.ScanLock_write

    # --------
    # Commands
    # --------

    @DebugIt()
    def dev_state(self):
        # PROTECTED REGION ID(HadesPress.State) ENABLED START #
        """
        This command gets the device state (stored in its device_state data member) and returns it to the caller.

        :return:'DevState'
        Device state
        """
        return DevState.UNKNOWN
        # PROTECTED REGION END #    //  HadesPress.State

    @DebugIt()
    def dev_status(self):
        # PROTECTED REGION ID(HadesPress.Status) ENABLED START #
        """
        This command gets the device status (stored in its device_status data member) and returns it to the caller.

        :return:'ConstDevString'
        Device status
        """
        return ""
        # PROTECTED REGION END #    //  HadesPress.Status

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the HadesPress module."""
    # PROTECTED REGION ID(HadesPress.main) ENABLED START #
    return run((HadesPress,), args=args, **kwargs)
    # PROTECTED REGION END #    //  HadesPress.main


if __name__ == '__main__':
    main()
