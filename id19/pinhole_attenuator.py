
import time
import sys

from bliss.common.tango import DevFailed
from bliss.comm.util import get_comm


class PinholeAttenuator:
    def __init__(self, name, config):
        self.__config = config
        
        self._wago = config.get("wago")
        self._cmd_key  = config.get("cmd_key")
        self._pos_key  = config.get("pos_key")
        
    #
    #     Attenuator to protect the pinhle slits
    #     Handled by a Wago
    #
    
    def att_in(self):
        value = self._wago.get(self._cmd_key) 
        value=1.0
        self._wago.set(self._cmd_key,value)
        value = self._wago.get(self._cmd_key) 
        while self.status() != "IN":
            time.sleep(0.5)
            print(self.status(),end='\r')
        
    def att_out(self):
        value = self._wago.get(self._cmd_key) 
        value=0.0
        self._wago.set(self._cmd_key,value)
        while self.status() != "OUT":
            time.sleep(0.5)
            print(self.status(),end='\r')
        
    def status(self):
        status = ""
        pos_word = self._wago.get(self._pos_key)
        if pos_word[1]==1.0:
            status += "OUT"
        else:
            if pos_word[0]==1.0:
                status += "IN"
            else:
                status += "Attenuator is MOVING!"
        return status
        
    def __info__(self):
        status = ""
        status += "Pinhole Attenuator state: "
        status += self.status()
        return status
        
    
