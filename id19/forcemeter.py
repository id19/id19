# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2017 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.comm.util import get_comm_type, get_comm, SERIAL
from bliss.comm.serial import SerialTimeout
from bliss.comm.util import get_comm

from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import SamplingCounterController

import time
import sys

class ForceMeter(SamplingCounterController):
    def __init__(self,name,config):
        super().__init__(name)
        
        opt = {"baudrate": 19200, "eol": b"\r"}
        self._cnx = get_comm(config, ctype=SERIAL, **opt)
        
        for counter_conf in config.get("counters", list()):
            cname = counter_conf["counter_name"]
            counter = self.create_counter(
                SamplingCounter, cname, mode=SamplingMode.SINGLE
            )
            
        self.max_retries = 3
        

    def read(self, counter):
        retry = 0
        while retry < self.max_retries:
            try:
                self._cnx.flush()
                
                #clear = b"\001" * 63
                #self._cnx.write(clear)
                
                ret = self._cnx.readline()
                #print (ret)
                ret = self._cnx.readline()
                #print (ret)
                
                try:
                    value = float(ret[3:10])
                # could not read counter value   
                except ValueError:
                    print (f"Prtocol error on forcemeter serial line! ret = {ret}")
                    ret = self._cnx.readline()
                    value = float(ret[3:10])
                #print (f"force value = {value}")
                
                return value
        
            except (SerialTimeout) as e:
                retry = retry + 1
                print (f"Timeout error on forcemeter serial line! Retry = {retry}")
                sys.excepthook(*sys.exc_info())

                if retry >= self.max_retries:
                    # re-throw the caught exception
                    raise e
