import PyTango as tango
import numpy as np

from bliss.controllers.counter import CalcCounterController

#
# Creates a 1D counter with the currents of all 4, 16 or 32 bunches.
# Switches with the SR filling mode. 
#

class BunchCurrentsCounterController(CalcCounterController):
    def __init__(self, name, config):
        super().__init__(name, config)
        
        self.dev_current_per_bunch = tango.DeviceProxy("acs.esrf.fr:10000/current-per-bunch")
        self.dev_machstat          = tango.DeviceProxy("acs.esrf.fr:10000/sys/machstat/tango")
        #self.dev_ct = tango.DeviceProxy("acs.esrf.fr:10000/srdiag/beam-current/total")
        
        self.sr_filling_mode    = self.dev_machstat.Filling_mode
        self.sr_filling_mode_id = self.dev_machstat.Filling_mode_id
        
        # all buches in SR
        self.bunches = 992
        
        # 4 bunch mode
        if self.sr_filling_mode_id == 6:
            self.bunches = 4
        # 16 bunch mode
        if self.sr_filling_mode_id == 7:
            self.bunches = 16
        # 32 bunch mode
        if self.sr_filling_mode_id == 8:
            self.bunches = 32
    
    def calc_function(self, input_dict):
        # example of self.inputs (2 input counters):
        #   [<....SimulationCounter object at 0x7f9da11f8350>,
        #    <....SimulationCounter object at 0x7f9da12537d0>]
        #
        # example of input_dict: {'data1': array([1.]), 'data2': array([1.])}
        #   * indexed by 'data1' and 'data2' tags.
        # example of self.outputs (1 output counter):
        #   [<bliss.common.counter.CalcCounter object at 0x7f03900a4e10>]
        #
        # example of self.tags: {'sim_ct_1': 'data1',
        #                        'sim_ct_2': 'data2',
        #                        'out1':     'out1'}
        # len(list(input_dict.values())[0])  is the number of points to compute.
        
        
        try:
            #I992_A = self.dev_ct.Current * self.dev_cpb.currents
            I992_A = input_dict[self.tags['current']] * self.dev_current_per_bunch.currents
            Ibunch_A = I992_A[0::int(992/self.bunches)]
        
            print(f"{self.sr_filling_mode}  :  {Ibunch_A}")
            return {self.tags[self.outputs[0].name]: [Ibunch_A]}
        
        except:
            raise RuntimeError ("Cannot get SR bunch currents. No SR current or no SR filling mode available!")
        

    @property
    def shape(self):
        return (self.bunches,)
