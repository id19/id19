import numpy as np
import time
import datetime
import os
from bliss.common.standard import *
from bliss.common.axis import *
from bliss.common.cleanup import *
from bliss.setup_globals import *
from bliss.config.static import get_config
from bliss import current_session

global short_calibration
short_calibration = False

def init_sx():
        
    sx.limits=(-100,100)
    
    ######### LIMIT 
    print("init_sx --- Moving to hard limit - ")
    start = time.time()
    sx.hw_limit(-1,wait=False)        
    
    
    time.sleep(1)
    string="init_sx --- Searching limit - ..."
    print(string)
    _status = sx.state
    while _status == "MOVING":
        _status = sx.state
        print(string)
        time.sleep(1)

    print("init_sx --- lim- found !\n")
    
    # hardware synchronization
    sx.sync_hard()

    _myposn = sx.position
    
    print("init_sx --- limit - value: %f" % (_myposn))
    
    if short_calibration:
        if sx.original_name == 'hrsx':
            _myposp = 38.912699 + _myposn
            mid = 38.912699 / 2.0 + _myposn
        if sx.original_name == 'mrsx':
            _myposp = - 49.26 + _myposn
            mid = - 49.26 / 2.0 + _myposn
    else:
        ######### LIMIT +
        print("init_sx --- Moving to hard limit +")

        sx.hw_limit(1,wait=False)
        
        time.sleep(1)
        string="init_sx --- Searching limit + ..."
        print(string)
        _status = sx.state
        while _status == "MOVING":
            _status = sx.state
            print(string)
            time.sleep(1)
            
        print("init_sx --- lim+ found !\n")
        
        # hardware synchronization
        sx.sync_hard()

        _myposp = sx.position
        
        print("init_sx --- Hard limit + value: %f" % (_myposp))
    
        mid = (_myposp - _myposn) / 2.0 + _myposn
    
    print("init_sx --- moving to middle position %f\n" % (mid))
    umv(sx,mid)
    
    print("init_sx --- reseting sx to 0\n")
    sx.dial = 0
    sx.position = 0
    
    slp = (_myposp - _myposn) / 2.0 - 0.05
    
    print("init_sx --- reseting sx limits to hard limits +/ 0.05\n")
    sx.limits=(-slp,slp)
    
    if time.time()-start >= 60:
        print("init_sx --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sx --- Time end: "+str(time.time()-start)+ ' sec')
    

# search for SY pos and neg limits and reset dial to 0 in the middle of them.
def init_sy():
    
    sy.limits=(-100,100)
    
    ######### LIMIT 
    print("init_sy --- Moving to hard limit - ")
    
    start = time.time()
    sy.hw_limit(-1,wait=False)
    
    time.sleep(1)
    string="init_sy --- Searching limit - ..."
    print(string)
    _status = sy.state
    while _status == "MOVING":
        _status = sy.state
        print(string)
        time.sleep(1)
        
    print("init_sy --- lim- found !\n")
    
    # hardware synchronization
    sy.sync_hard()

    _myposn = sy.position
    
    print("init_sy --- Hard limit - value: %f" % (_myposn))
    
    if short_calibration:
        if sy.original_name == 'hrsy':
            _myposp = _myposn - 38.818497  
            mid = -38.818497 / 2.0 + _myposn
        if sy.original_name == 'mrsy':
            _myposp = _myposn + 48.11675
            mid = 48.11675 / 2.0 + _myposn
    else:
        ######### LIMIT +
        print("init_sy --- Moving to hard limit +")
        sy.hw_limit(1,wait=False)
        
        time.sleep(1)
        string="init_sy --- Searching limit + ..."
        print(string)
        _status = sy.state
        while _status == "MOVING":
            _status = sy.state
            print(string)
            time.sleep(1)
        print("init_sy --- lim+ found !\n")
        
        
        # hardware synchronization
        sy.sync_hard()

        _myposp = sy.position
        
        print("init_sy --- Hard limit + value: %f" % (_myposp))
        
        mid = (_myposp - _myposn) / 2.0 + _myposn
    
    print("init_sy --- moving to middle position %f\n" % (mid))
    umv(sy,mid)
    
    print("init_sy --- reseting sy to 0\n")
    
    sy.dial = 0
    sy.position = 0
    
    slp = (_myposp - _myposn) / 2.0 - 0.05
    
    print("init_sy --- reseting sy limits to hard limits +/ 0.05\n")
    sy.limits=(-slp,slp)
    
    if time.time()-start >= 60:
        print("init_sy --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sy --- Time end: "+str(time.time()-start)+ ' sec')

# search for SZ pos home switch and reset dial to 0
def init_mr_yrot():
    yrot.position = 0
    yrot.dial = 0
    previous_speed = yrot.velocity
    yrot.velocity = 5
    yrot.limits=(-10000,10000)
    
    ######### LIMIT -
    print("init_yrot --- Moving to hard limit - ")
    start = time.time()
    try:
        mvd(yrot,-999)
    except:
        if 'LIMNEG' in yrot.state or 'LIMPOS' in yrot.state:

            print("init_yrot --- lim- found !\n")
    
            # hardware synchronization
            yrot.sync_hard()
            
            _myposn = yrot.position
            
            print("init_yrot --- Hard limit - value: %f" % (_myposn))
    
    
    ######### LIMIT +
    print("init_yrot --- Moving to hard limit + ")
    start = time.time()
    try:
        mvd(yrot,999)
    except:
        if 'LIMNEG' in yrot.state or 'LIMPOS' in yrot.state:

            print("init_yrot --- lim+ found !\n")
            
            # hardware synchronization
            yrot.sync_hard()
            
            _myposp = yrot.position
            
            print("init_yrot --- Hard limit + value: %f" % (_myposp))
    
    mid = (_myposp -  _myposn) / 2.0 + _myposn
    
    print("init_yrot --- moving to middle position %f\n" % (mid))
    umv(yrot,mid)
    
    print("init_yrot --- reseting yrot to 0\n")
    
    yrot.dial = 0
    yrot.position = 0
    
    slp = (_myposp - _myposn) / 2.0 - 0.05
    
    print("init_yrot --- reseting yrot limits to hard limits +/ 0.05\n")
    yrot.limits=(-slp,slp)
    yrot.velocity = previous_speed
    
    if time.time()-start >= 60:
        print("init_yrot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_yrot --- Time end: "+str(time.time()-start)+ ' sec')

# search for YROT pos and neg limits and reset dial to 0 in the middle of them.
def init_hr_yrot():
    
    yrot.limits=(-1000,1000)
    
    ######### LIMIT 
    print("init_yrot --- Moving to hard limit - ")
    start = time.time()
    yrot.hw_limit(-1,wait=False)
    
    
    time.sleep(1)
    string="init_yrot --- Searching limit - ..."
    print(string)
    _status = yrot.state
    while _status == "MOVING":
        _status = yrot.state
        print(string)
        time.sleep(1)

    print("init_yrot --- lim- found !\n")
    
    # hardware synchronization
    yrot.sync_hard()

    _myposn = yrot.position
    
    print("init_yrot --- Hard limit - value: %f" % (_myposn))
    
    ######### LIMIT +
    print("init_yrot --- Moving to hard limit +")
    yrot.hw_limit(1,wait=False)
    
    
    time.sleep(1)
    string="init_yrot --- Searching limit + ..."
    print(string)
    _status = yrot.state
    while _status == "MOVING":
        _status = yrot.state
        print(string)
        time.sleep(1)


    print("init_yrot --- lim+ found !\n")
    
    # hardware synchronization
    yrot.sync_hard()
    
    _myposp = yrot.position
    
    print("init_yrot --- Hard limit + value: %f" % (_myposp))
    
    mid = (_myposp -  _myposn) / 2.0 + _myposn
    
    print("init_yrot --- moving to middle position %f\n" % (mid))
    umv(yrot,mid)
    
    print("init_yrot --- reseting yrot to 0\n")
    
    yrot.dial = 0
    yrot.position = 0
    
    slp = (_myposp - _myposn) / 2.0 - 0.05
    
    print("init_yrot --- reseting yrot limits to hard limits +/ 0.05\n")
    yrot.limits=(-slp,slp)
    
    if time.time()-start >= 60:
        print("init_yrot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_yrot --- Time end: "+str(time.time()-start)+ ' sec')

# search for YROT pos home switch and reset dial to 0
def init_mr_sz():
    sz.position = 0
    sz.dial = 0
    sz.limits=(-10000,10000)
    
    ######### LIMIT -
    print("init_sz --- Moving to hard limit - ")
    start = time.time()
    try:
        mvd(sz,-999)
    except:
        if 'LIMNEG' in sz.state:
            
            print("init_sz --- lim- found !\n")
    
            # hardware synchronization
            sz.sync_hard()
    
            _myposn = sz.position

            print("init_sz --- Hard limit - value: %f" % (_myposn))
    
    ######### LIMIT +
    print("init_sz --- Moving to hard limit + ")
    start = time.time()
    try:
        mvd(sz,999)
    except:
        if 'LIMPOS' in sz.state:

            print("init_sz --- lim+ found !\n")
            
            # hardware synchronization
            sz.sync_hard()
            
            _myposp = sz.position
            
            print("init_sz --- Hard limit + value: %f" % (_myposp))
    
    mid = (_myposp -  _myposn) / 2.0 + _myposn
    
    print("init_sz --- moving to middle position %f\n" % (mid))
    umvd(sz,mid)
    
    print("init_sz --- reseting sz to 0\n")
    
    sz.position = 0
    sz.dial = 0
    
    slp = (_myposp - _myposn) / 2.0 - 0.05
    
    print("init_sz --- reseting sz limits to hard limits +/ 0.05\n")
    sz.limits=(-slp,slp)
    
    if time.time()-start >= 60:
        print("init_sz --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sz --- Time end: "+str(time.time()-start)+ ' sec')
    
def init_hr_sz():

    lim_n = sz.limits[0]
    sz.limits=(lim_n,100)
    
    ######### LIMIT +
    print("init_sz --- Moving to hard limit +")
    start = time.time()
    sz.hw_limit(1,wait=False)
    
    
    time.sleep(1)
    string="init_sz --- Searching limit + ..."
    print(string)
    _status = sz.state
    while _status == "MOVING":
        _status = sz.state
        print(string)
        time.sleep(1)

    print("init_sz --- lim+ found !\n")
    
    # hardware synchronization
    sz.sync_hard()

    _myposp = sz.position
    
    print("init_sz --- Hard limit + value: %f" % (_myposp))
    
    cal_pos = 40.0
    slp = cal_pos - 0.05
    print("init_sz --- reseting sz to %f\n" % (cal_pos))
    
    sz.dial = cal_pos
    sz.position = cal_pos
    
    print("init_sz --- moving to calibration position %f\n" % (cal_pos - 10.0))
    umvr(sz,-10.0)
    
    print("init_sz --- reseting sz limit+ to hard limit - 0.05\n")
    sz.limits=(lim_n,slp)
    
    if time.time()-start >= 60:
        print("init_sz --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_sz --- Time end: "+str(time.time()-start)+ ' sec')


def init_hr_z0():

    lim_p = z0.limits[1]
    z0.limits=(-100,lim_p)
    
    ######### LIMIT 
    print("init_z0 --- Moving to hard limit -")
    start = time.time()
    z0.hw_limit(-1,wait=False)
    
    
    time.sleep(1)
    string="init_z0 --- Searching limit - ..."
    print(string)
    _status = z0.state
    while _status == "MOVING":
        _status = z0.state
        print(string)
        time.sleep(1)

    print("init_z0 --- lim- found !\n")
    
    # hardware synchronization
    z0.sync_hard()
    
    _myposn = z0.position
    
    print("init_z0 --- Hard limit - value: %f" % (_myposn))
    
    cal_pos = 0.0
    sln = cal_pos + 0.05
    
    print("init_z0 --- reseting z0 to %f\n" % (cal_pos))
    z0.dial = cal_pos
    z0.position = cal_pos
    
    print("init_z0 --- moving to calibration position %f\n" % (cal_pos + 10.0))
    umvr(z0,10.0)
    
    print("init_z0 --- reseting z0 limit to hard limit + 0.05\n")
    z0.limits=(sln,lim_p)
    
    if time.time()-start >= 60:
        print("init_z0 --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_z0 --- Time end: "+str(time.time()-start)+ ' sec')

# search for SROT pos home switch and reset dial to 0
def init_mr_srot():
    srot.dial = 0
    srot.position = 0
    
    start=time.time()

    print("init_srot --- Searching home...")
    srot.home()
    
    time.sleep(0.5)
    srot.sync_hard()
    # move some degrees to free the home switch. Only moving to 0, the home switch will still be active
    # and another call to home will end in eternal rotation! 
    mv(srot,5)
    print("init_srot --- moving srot to 0\n")
    umv(srot,180)
    srot.position=0
    srot.dial=0
    
    if time.time()-start >= 60:
        print("init_srot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_srot --- Time end: "+str(time.time()-start)+ ' sec')

# search for SROT pos home switch, go to user position and reset dial to 0
def init_hr_srot():
    
    srot.dial = 0
    srot.position = 0
    
    pos = 248
    start=time.time()
    srot.controller.home_search(srot,1,pos)
    
    srot.sync_hard()
    string="init_srot --- Searching home..."
    print(string)
    srot.controller.home_state(srot)
    while "READY" not in srot.state:
        srot.controller.home_state(srot)
        print(string)
        time.sleep(1)
    
    print("init_srot --- moving srot to 0\n")
    umv(srot,0)
    
    if time.time()-start >= 60:
        print("init_srot --- Time end: "+str((time.time()-start)/60) + ' min')
    else:
        print("init_srot --- Time end: "+str(time.time()-start)+ ' sec')
    
   
def calib_motors(*motors,short_calib=True):
    
    all_calib = [init_sx,init_sy]
    all_motors = [sx,sy,sz,yrot,srot]
    if current_session.name == 'HRTOMO':
        all_calib += [init_hr_sz,init_hr_yrot,init_hr_srot,init_hr_z0]
        all_motors.append(z0)
    if current_session.name == 'MRTOMO':
        all_calib += [init_mr_sz,init_mr_yrot,init_mr_srot]
        
    global short_calibration
    short_calibration = short_calib
    if len(motors) > 0:
        calibrations = []
        for mot in motors:
            calibrations.append(all_calib[all_motors.index(mot)])
    else:
        calibrations = all_calib 
    # context manager used to stop motors when an exception is raised
    with error_cleanup(*all_motors):
        t0 = time.time()
        cal=[gevent.spawn(calib) for calib in calibrations]
        try:
            # wait for all calibrations to be done
            gevent.joinall(cal)
            tend = time.time() - t0
            if tend >= 60:
                print(f"Calibration took {tend/60} min")
            else:
                print(f"Calibration took {tend} sec")
        except:
            gevent.killall(cal)
            print("\n")
            raise
