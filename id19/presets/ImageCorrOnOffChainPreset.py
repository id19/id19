from bliss.setup_globals import *
from bliss.common.logtools import log_info, log_debug, log_warning
from bliss.scanning.chain import ChainPreset,ChainIterationPreset
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster

#
# ATTENTION: This preset is not working! All action to modify the desctor acquisition comes to late!
#             Using the proxy, blocks the scan!!!!
#

class ImageCorrOnOffChainPreset(ChainPreset):
    """
    Class used for all bliss common scans (ct, ascan, timescan,...) to switch of the image correction, 
    for all detectors in the measurement group, when image saving is requested.
    
    After acquisition, the image correction mode is restored.
    """  
    def prepare(self,acq_chain):
        
        lima_acq_obj = [acq_obj for acq_obj in acq_chain.nodes_list if type(acq_obj) == LimaAcquisitionMaster]
        
        self.use_background    = {}
        self.use_flatfield     = {}
        self.detectors = []

        for acq_obj in lima_acq_obj:
            if acq_obj.acq_params["saving_mode"] != "NOSAVING":
                self.detectors.append(acq_obj.device)
                self.use_background[acq_obj.device] = False
                self.use_flatfield[acq_obj.device]  = False
            else:
                if acq_obj.device.processing.use_background:
                    log_warning(self,f'background_substraction is activated on {acq_obj.device.name}\n')
                if acq_obj.device.processing.use_flatfield:
                    log_warning(self,f'flatfield is activated on {acq_obj.device.name}\n')

    def start(self,acq_chain):
        for detector in self.detectors:
            print ("corr off")
            if detector.processing.use_background:
                self.use_background[detector] = True
                detector.processing.use_background = False
                #bg_proxy = detector._get_proxy("backgroundsubstraction")
                #bg_proxy.stop()
            
            if detector.processing.use_flatfield:
                self.use_flatfield[detector] = True 
                detector.processing.use_flatfield = False
            
    def stop(self,acq_chain):
        for detector in self.detectors:
            print ("corr on")
            if self.use_background[detector]:
                detector.processing.use_background = True
                #bg_proxy = detector._get_proxy("backgroundsubstraction")
                #bg_proxy.start()
                
            if self.use_flatfield[detector]:
                detector.processing.use_flatfield = True
        
