from bliss.setup_globals import *
from bliss.scanning.chain import ChainPreset,ChainIterationPreset

class FastShutterChainPreset(ChainPreset):
    """
    Class used for all bliss common scans (ct, ascan, timescan,...) to open the fast shutter before taking each image 
    and close it after acquisition.
    
    ***Attributes***
    shutter : Fast shutter object
        contains methods to control shutters
    """
    def __init__(self, fshutter):
        self.shutter = fshutter
        
    class Iterator(ChainIterationPreset):
        def __init__(self,shutter,iteration_nb):
            self.shutter = shutter
            self.iteration = iteration_nb
        
        def start(self):
            """
            Opens shutter if shutter controlled by soft
            """
            self.shutter.open()
    
        def stop(self):
            """
            Closes shutter if shutter controlled by soft
            """
            self.shutter.close()
                
    def get_iterator(self,acq_chain):
        """
        Generates fastshutter preset for each acquisition chain iteration. 
        It means at each image for bliss common scans.
        """
        iteration_nb = 0
        while True:
            yield FastShutterChainPreset.Iterator(self.shutter,iteration_nb)
            iteration_nb += 1
