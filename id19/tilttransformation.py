# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
"""
Virtual X and Y axis which stay on top of a rotation
"""

from bliss.controllers.motor import CalcController
from bliss.common.utils import object_method
import pint
import gevent
from bliss.shell.standard import *
from bliss.common.hook import MotionHook
from bliss import current_session
import numpy as np
import time
from math import isclose


class MicosHook(MotionHook):
    """pushers rotation motion hook"""

    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.pusher = config['pusher']
        super().__init__()

    def pre_move(self, motion_list):
        self.pusher.move_out()
        gevent.sleep(1)

    #def post_move(self, motion_list):
        #gevent.sleep(2)
    

class Pusher():
    def __init__(self, name, config):
        self.name = name
        self._config  = config
        self.air_ctrl = config.get("air_ctrl") #Sam
        self.sx45 = config.get("sx45")
        self.sy45 = config.get("sy45")
        self._sx = config.get("sx")
        self._sy = config.get("sy")
        magnets = config.get("magnets")
        self.mx45 = magnets[0]
        self.my45 = magnets[1]

        
    def __info__(self):
        status = ""
        status += "Positions:\n"
        status += "sx45: \n"
        status += "\tposition= " + str(self.sx45.position) + "\n"
        status += "\tin position= " + str(self.sx) + "\n"
        status += "sy45: \n"
        status += "\tposition= " + str(self.sy45.position) + "\n"
        status += "\tin position= " + str(self.sy) + "\n"
        
        status += "Air state:\n"
        status += "\t" + str("On" if self.check_magnets_on() else "Off") + "\n"
        # How to read air state?
        
        status += "Magnet states:\n"
        status += "mx45: \n"
        status += self.mx45.__info__() + "\n"
        status += "my45: \n"
        status += self.my45.__info__() + "\n"
        
        return status

    @property
    def sx(self):
        return self._sx
        
    @sx.setter
    def sx(self, value):
        self._config["sx"] = value
        self._config.save()
        self._sx = value

    @property
    def sy(self):
        return self._sy
        
    @sy.setter
    def sy(self, value):
        self._config["sy"] = value
        self._config.save()
        self._sy = value

    #def pusher_air_on(self):
    def air_on(self):
        self.air_ctrl.raw_write(self.sx45, "Dmc2143 sendcommand SB 1")

    #def pusher_air_off(self):
    def air_off(self):
        self.air_ctrl.raw_write(self.sx45, "Dmc2143 sendcommand CB 1") 

    def magnets_on(self):
        self.air_on()
        self.mx45.on()
        self.my45.on()
    
    def magnets_off(self):
        self.air_off()
        self.mx45.off()
        self.my45.off()
        
    def magnets_on_position(self, value):
        self.mx45.positions_list[0]["target"][0]["destination"] = value 
        self.mx45.update_position(label="on", motors_destinations_list=[value])
        self.my45.positions_list[0]["target"][0]["destination"] = value
        self.my45.update_position(label="on", motors_destinations_list=[value])
        
    def magnets_on_position_x(self, value):
        self.mx45.positions_list[0]["target"][0]["destination"] = value 
        self.mx45.update_position(label="on", motors_destinations_list=[value])
        
    def magnets_on_position_y(self, value):
        self.my45.positions_list[0]["target"][0]["destination"] = value
        self.my45.update_position(label="on", motors_destinations_list=[value])
    
    
    def check_magnets_on(self):
        if self.mx45.position == "on" and self.my45.position == "on":
            return True
        else:
            return False
        
    def _check_rotation(self):
        config = current_session.config
        rl = config.get("rl")
        
        #Check with tolerance!!!!!!
        if not isclose(rl.dial,0, abs_tol=rl.tolerance):
            raise RuntimeError("Cannot move pushers if rotation is not at 0")

    #def pushers_move_x(self, rel_pos):
    def move_x(self, rel_pos):
        self._check_rotation()
        q = pint.UnitRegistry()
        q.define('pixel = 1 * count = px')
        q.define('fraction = [] = frac')
        q.define('percent = 1e-2 frac = pct')
        ALPHA = 90 * q.deg
        BETA = 90 * q.deg
        GAMMA = 135 * q.deg
        offset = 0 * q.deg
        
        sx45_pos = self.sx45.position * q.mm
        sy45_pos = self.sy45.position * q.mm
        rel_pos = rel_pos * q.mm

        # calculate target positions, x/y controlled by offset
        sx45_target = sx45_pos + (rel_pos / np.cos(GAMMA - offset))
        sy45_target = sy45_pos + (rel_pos / np.cos(GAMMA + BETA - offset))

        # check air and magnet state before moving!!!!
        if not self.check_magnets_on():
            self.magnets_on()
        umv(self.sx45, sx45_target.magnitude, self.sy45, sy45_target.magnitude)
        #self.magnets_off()
        #self.pusher_air_off()

    #def pushers_move_y(self, rel_pos):
    def move_y(self, rel_pos):
        self._check_rotation()
        q = pint.UnitRegistry()
        q.define('pixel = 1 * count = px')
        q.define('fraction = [] = frac')
        q.define('percent = 1e-2 frac = pct')
        ALPHA = 90 * q.deg
        BETA = 90 * q.deg
        GAMMA = 135 * q.deg
        offset = ALPHA
        
        sx45_pos = self.sx45.position * q.mm
        sy45_pos = self.sy45.position * q.mm
        rel_pos = rel_pos * q.mm

        # calculate target positions, x/y controlled by offset
        sx45_target = sx45_pos + (rel_pos / np.cos(GAMMA - offset))
        sy45_target = sy45_pos + (rel_pos / np.cos(GAMMA + BETA - offset))

        # check air and magnet state before moving!!!!!!
        if not self.check_magnets_on():
            self.magnets_on()
        umv(self.sx45, sx45_target.magnitude, self.sy45, sy45_target.magnitude)
        #self.magnets_off()
        #self.pusher_air_off()

    def move_out(self):
        if not isclose(self.sx45.position,0,abs_tol=self.sx45.tolerance) or not isclose(self.sy45.position,0,abs_tol=self.sy45.tolerance):
            self.sx = self.sx45.position
            self.sy = self.sy45.position
            self.magnets_off()
            umv(self.sx45,0, self.sy45, 0)
    
    def move_in(self):
        self._check_rotation()
        umv(self.sx45,self.sx, self.sy45, self.sy)
        self.magnets_on()
        #self.magnets_off()
        #self.pusher_air_off()




