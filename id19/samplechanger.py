import numpy as np
import time
import datetime
import os
import gevent
from tabulate import tabulate
import sys
from bliss import current_session
from bliss.common.standard import *
from bliss.common.axis import *
from bliss.shell.standard import *
from bliss.common.cleanup import *
from bliss.common.scans import *
from bliss.setup_globals import *
from bliss import setup_globals
from bliss.config.static import get_config

from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog,display

from tango.gevent import DeviceProxy

class SampleChanger:
    def __init__(self, name, config):

        tango_server = config['tango_server'][0]
        self.samples_device = DeviceProxy(tango_server['samples'])
        self.robot_device = DeviceProxy(tango_server['robot'])
        self.shutter = config.get('shutter')

        if 'MRTOMO' in current_session.name:
            self.fulltomo = config['tomo'][1]['MR']['fulltomo']
            self.zseries = config['tomo'][1]['MR']['zseries']
            mr_positions = config['positions'][1]['MR']
            
            robot_positions = mr_positions['robot'].to_dict()
            experiment_positions = mr_positions['experiment'].to_dict()
            axis = config['axis'][1]['MR']
                
        elif 'HRTOMO' in current_session.name:
            self.fulltomo = config['tomo'][0]['HR']['fulltomo']
            self.zseries = config['tomo'][0]['HR']['zseries']
            hr_positions = config['positions'][0]['HR']
            
            robot_positions = hr_positions['robot'].to_dict()
            experiment_positions = hr_positions['experiment'].to_dict()
            axis = config['axis'][0]['HR']
            
        else:
            print('No sample change positions defined for this setup')
            return

        self.robot_positions = robot_positions
        self.experiment_positions = experiment_positions
        self.axis = axis
        self.axis_name = [(key, motor.name)for key, motor in self.axis.items()]

        self.tomo_det = config['tomo'][2]['detector']['xray']
        self.vis_det = config['tomo'][2]['detector']['visible']

        self.sample_positions=list()
        for col in ['A','B','C']:
            for pos in range(1,19):
                if pos not in [7,8]:
                    self.sample_positions.append(col+str(pos))


        # list of scan sequence associated to each sample (fasttomo180, fasttomo360, zseries)
        self.tomo_sequence = []
        self._config = config

    def __info__(self):

        info_str = "samplechanger info\n"
        info_str += f" samples_device = {self.samples_device}\n"
        info_str += f" robot_device = {self.robot_device}\n"
        info_str += f" tomo_station = {current_session.name}\n"
        info_str += f" robot_positions = {self.robot_positions}\n"
        info_str += f" experiment_positions = {self.experiment_positions}\n"
        info_str += f" axis = {self.axis_name}\n"
        info_str += f" active sample position = {self.robot_device.SamplePosition}\n"
        info_str += f" remote mode active = {self.robot_device.RemoteMode}\n"
        info_str += f" air pressure ok = {self.robot_device.AirPressureOK}\n"
        info_str += f" robot aligned = {self.robot_device.Aligned}\n"
        info_str += f" safety chain ok = {self.robot_device.SafetyChainOK}\n"
        info_str += f" sample loaded = {self.robot_device.SampleLoaded}\n"
        info_str += f" wago ok = {self.robot_device.WagoOK}\n"
        info_str += f" parking position = {self.robot_device.ParkingPosition}\n"
        info_str += f" robot status = {self.robot_device.State()}\n"
        return info_str

    #
    # Add a list of samples
    #
    def add_samples(self, sample_name=None, sample_position=None):

        if sample_name is not None and sample_position is not None:
            sample = list()
            sample.append(sample_position)
            sample.append(sample_name)
            self._valid_sample(sample_position,'add')
            try:
                self.samples_device.AddSample(sample)
            except Exception as exception:
                self._tango_error(exception)
                return
        else:
            dlg_sample_size = UserCheckBox(label="Enter sample sizes", defval=False)

            dlg_message  = UserMsg(label="The sample positions A,B,C 7 and 8 do not exist on the sample changer shelf!")

            validator = Validator(self._valid_sample,'add')
            dlg_sample_position = UserInput(label="Sample position", defval='A1', validator=validator)
            dlg_sample_name = UserInput(label="Sample name", defval="Name")

            dlg_next = UserCheckBox(label="Next sample", defval=False)

            ret = True
            while ret != False:

                ret = BlissDialog( [[dlg_sample_size],[dlg_message],[dlg_sample_position],[dlg_sample_name],[dlg_next]], title='Add samples').show()

                if ret != False:
                    sample = list()
                    sample.append(ret[dlg_sample_position])
                    sample.append(ret[dlg_sample_name])

                    try:
                        self.samples_device.AddSample(sample)
                    except Exception as exception:
                        self._tango_error(exception)
                        return

                    if ret[dlg_sample_size]:
                        dlg_size_x = UserFloatInput(label="Sample size in X (mm)")
                        dlg_size_y = UserFloatInput(label="Sample size in Y (mm)")
                        dlg_size_z = UserFloatInput(label="Sample size in Z (mm)")

                        ret2 = BlissDialog( [[dlg_size_x],[dlg_size_y],[dlg_size_z]], title='Add samples').show()

                        if ret2 != False:
                            sample_size = list()
                            sample_size.append(ret2[dlg_size_x])
                            sample_size.append(ret2[dlg_size_y])
                            sample_size.append(ret2[dlg_size_z])

                            try:
                                self.samples_device.ActivateSample(sample[0])
                            except Exception as exception:
                                self._tango_error(exception)
                                return
                            try:
                                self.samples_device.ActiveSampleSize = sample_size
                            except Exception as exception:
                                self._tango_error(exception)
                                return

                    if not ret[dlg_next]:
                        break

                    # this allows to suggest to user sample position following last sample position added
                    dlg_sample_position = UserInput(label="Sample position", defval=self.sample_positions[self.sample_positions.index(ret[dlg_sample_position])+1])
                    dlg_sample_name = UserInput(label="Sample name", defval="Name")

        self.show_samples()

    #
    # Add a list of samples without user dialog
    # Samples will have a common prefix name followed by an increasing number
    # Samples will have same aligned position for axis sz
    #
    def add_default_samples(self,nb_samples,prefix,align_sz):

        for nb in range(nb_samples):
            sample = list()
            sample_pos = self.sample_positions[nb]
            sample.append(sample_pos)
            if nb < 9:
                # name ex: 'prefixO1'
                sample_name = prefix+'0'+str(nb+1)
            else:
                # name ex: 'prefix10'
                sample_name = prefix+str(nb+1)
            sample.append(sample_name)

            try:
                self.samples_device.AddSample(sample)
            except Exception as exception:
                self._tango_error(exception)
                return
            self.experiment_positions['sz'] = align_sz

    #
    # Delete a sample by specifying its name or position
    #
    def delete_samples(self, sample_name=None, sample_position=None):
        if sample_name is not None or sample_position is not None:

            if sample_name is not None:
                sample = sample_name
            else:
                sample = sample_position
            self._valid_sample(sample,'delete')
            try:
                self.samples_device.DeleteSample(sample)
            except Exception as exception:
                self._tango_error(exception)
                return
        else:
            validator = Validator(self._valid_sample,'delete')
            dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)

            ret = BlissDialog([[dlg_sample]],title='Delete sample').show()

            if ret != False:
                sample = ret[dlg_sample]
                dlg_confirm = UserMsg(label=f"The sample {ret[dlg_sample]} is going to be deleted. Continue?")
                ret = BlissDialog([[dlg_confirm]],title='Delete sample').show()
                if ret != False:
                    try:
                        self.samples_device.DeleteSample(sample)
                    except Exception as exception:
                        self._tango_error(exception)
                        return

    #
    # Modify a sample by specifying its name or position
    # All attributes (position, name, size and align position) can be modified
    #
    def edit_samples(self, sample_name=None, sample_position=None, size_x=None, size_y=None, size_z=None, align_pos_x=None, align_pos_y=None, align_pos_z=None):
        attrdict = {'sample_position':'Position','sample_name':'Name','size_x':'Size X','size_y':'Size Y','size_z':'Size Z','align_pos_x':'Align SX','align_pos_y':'Align SY','align_pos_z':'Align SZ'}
        if sample_name is not None and sample_position is not None:
            try:
                lsamples = list(self.samples_device.Samples)
                if sample_name in lsamples:
                    attr = list()
                    attr.append('Position')
                    attr.append(sample_position)
                    # retrieve sample position from its name
                    sample_oldposition = lsamples[lsamples.index(sample_name)-1]
                    # if given attribute is position, sample activation is done is EditSample command
                    self.samples_device.ActivateSample(sample_oldposition)
                    self.samples_device.EditSample(attr)
                elif sample_position in lsamples:
                    attr = list()
                    attr.append('Name')
                    attr.append(sample_name)
                    self.samples_device.ActivateSample(sample_position)
                    self.samples_device.EditSample(attr)
                    self.samples_device.ActivateSample(sample_position)
                else:
                    raise ValueError(f"Sample position {sample_position} and sample name {sample_name} do not exist")
            except Exception as exception:
                self._tango_error(exception)
                return
        elif (sample_name is not None or sample_position is not None) and (size_x is not None or size_y is not None or size_z is not None or align_pos_x is not None or align_pos_y is not None or align_pos_z is not None):
            if sample_name is not None:
                self._valid_sample(sample_name,'edit')
                try:
                    lsamples = list(self.samples_device.Samples)
                    # retrieve sample position from its name
                    sample_position = lsamples[lsamples.index(sample_name)-1]
                    # if given attribute is position, sample activation is done is EditSample command
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
            else:
                self._valid_sample(sample_position,'edit')
                try:
                    # if given attribute is position, sample activation is done is EditSample command
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return


            for attr_value in [size_x,size_y,size_z,align_pos_x,align_pos_y,align_pos_z]:
                try:
                    # retrieve existing key in 'sample' dictionary from 'lattr' list values
                    attr_name = attrdict.get(str(attr_value))
                    attr = list()
                    attr.append(attr_name)
                    attr.append(attr_value)
                    self.samples_device.EditSample(attr)
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
        else:
            validator = Validator(self._valid_sample,'edit')
            dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)

            ret = BlissDialog([[dlg_sample]],title='Edit sample').show()

            if ret is False:
                return
            else:
                sample={}
                if ret[dlg_sample] in self.sample_positions:
                    sample['position'] = ret[dlg_sample]
                    try:
                        sample['name'] = self.samples_device.ActiveSampleName
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                else:
                    sample['name'] = ret[dlg_sample]
                    try:
                        lsamples = list(self.samples_device.Samples)
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    # retrieve sample position from its name
                    sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]

                # retrieve all sample attributes from its position
                try:
                    # select a sample from its position
                    self.samples_device.ActivateSample(sample['position'])
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['sizex'] = self.samples_device.ActiveSampleSize[0]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['sizey'] = self.samples_device.ActiveSampleSize[1]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['sizez'] = self.samples_device.ActiveSampleSize[2]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['alignsx'] = self.samples_device.ActiveSampleAlignPos[0]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['alignsy'] = self.samples_device.ActiveSampleAlignPos[1]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['alignsz'] = self.samples_device.ActiveSampleAlignPos[2]
                except Exception as exception:
                    self._tango_error(exception)
                    return

                lattr = [(0,'Position'),(1,'Name'),(2,'Size X'),(3,'Size Y'),(4,'Size Z'),(5,'Align SX'),(6,'Align SY'),(7,'Align SZ')]
                dlg_att_name = UserChoice(values=lattr,defval=0)
                ret = BlissDialog([[dlg_att_name]],title='Edit sample').show()
                if ret != False:
                    attr = list()
                    # retrieve existing key in 'sample' dictionary from 'lattr' list values
                    key = lattr[ret[dlg_att_name]][1].replace(' ','')
                    attr.append(key)
                    # get actual value of attribute
                    defval = sample[key.lower()]
                    dlg_att_value = UserInput(label=f"{lattr[ret[dlg_att_name]][1]}",defval=defval)
                    ret = BlissDialog([[dlg_att_value]],title='Edit sample').show()
                    if ret != False:
                        attr.append(ret[dlg_att_value])
                        try:
                            # EditSample command is waiting for an array composed of attribute name and value
                            self.samples_device.EditSample(attr)
                        except Exception as exception:
                            self._tango_error(exception)
                            return
                        if attr[0] != 'Position':
                            try:
                                # if given attribute is position, sample activation is done is EditSample command
                                self.samples_device.ActivateSample(sample['position'])
                            except Exception as exception:
                                self._tango_error(exception)
                                return

        # display all sample attributes to user
        try:
            sample = dict()
            sample['position'] = self.samples_device.ActiveSamplePosition
            sample['name'] = self.samples_device.ActiveSampleName
            sample['sizex'] = self.samples_device.ActiveSampleSize[0]
            sample['sizey'] = self.samples_device.ActiveSampleSize[1]
            sample['sizez'] = self.samples_device.ActiveSampleSize[2]
            sample['alignsx'] = self.samples_device.ActiveSampleAlignPos[0]
            sample['alignsy'] = self.samples_device.ActiveSampleAlignPos[1]
            sample['alignsz'] = self.samples_device.ActiveSampleAlignPos[2]
        except Exception as exception:
            self._tango_error(exception)
            return
        for attr_name,attr_value in sample.items():
            print(f"{attr_name}: {attr_value}")

        print("\nYour change is effective")

    def write_align_pos_for_all_samples(self, sx, sy, sz):
        try:
            lsamples = list(self.samples_device.Samples)
            for sample_pos_idx in range(0,len(lsamples),2):
                # select a sample from its position
                self.samples_device.ActivateSample(lsamples[sample_pos_idx])
                align_pos = [sx,sy,sz]
                self.samples_device.ActiveSampleAlignPos = align_pos
                self.samples_device.ActiveAligned = True
        except Exception as exception:
            self._tango_error(exception)
            return


    #
    # Read all dimensions of a sample and return the values
    #
    def read_sample_size(self, sample_name=None, sample_position=None):

        if sample_name is not None or sample_position is not None:
            if sample_name is not None:
                self._valid_sample(sample_name,'read')
                try:
                    lsamples = list(self.samples_device.Samples)
                    # retrieve sample position from its name
                    sample_position = lsamples[lsamples.index(sample_name)-1]
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
            elif sample_position is not None:
                self._valid_sample(sample_position,'read')
                try:
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
            try:
                sample = {}
                sample['sizex'] = self.samples_device.ActiveSampleSize[0]
                sample['sizey'] = self.samples_device.ActiveSampleSize[1]
                sample['sizez'] = self.samples_device.ActiveSampleSize[2]
            except Exception as exception:
                self._tango_error(exception)
                return

            # display all sample dimensions to user
            print('\n')
            for dim in ['X','Y','Z']:
                key = 'size'+dim.lower()
                print(f"Sample size in {dim} (mm): {sample[key]}")
            print('\n')

        else:
            validator = Validator(self._valid_sample,'read')
            dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)

            ret = BlissDialog([[dlg_sample]],title='Read sample size').show()

            if ret != False:

                sample={}
                if ret[dlg_sample] in self.sample_positions:
                    sample['position'] = ret[dlg_sample]
                else:
                    try:
                        lsamples = list(self.samples_device.Samples)
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    # retrieve sample position from its name
                    sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]

                try:
                    # select a sample from its position
                    self.samples_device.ActivateSample(sample['position'])
                except Exception as exception:
                    self._tango_error(exception)
                    return

                try:
                    sample['sizex'] = self.samples_device.ActiveSampleSize[0]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['sizey'] = self.samples_device.ActiveSampleSize[1]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['sizez'] = self.samples_device.ActiveSampleSize[2]
                except Exception as exception:
                    self._tango_error(exception)
                    return

                # display all sample dimensions to user
                print('\n')
                for dim in ['X','Y','Z']:
                    key = 'size'+dim.lower()
                    print(f"Sample size in {dim} (mm): {sample[key]}")
                print('\n')

    #
    # Define sample size in X,Y and Z dimensions
    #
    def write_sample_size(self, sample_name=None, sample_position=None, sample_size_x=None, sample_size_y=None, sample_size_z=None):

        if sample_name is not None or sample_position is not None:
            if sample_name is not None:
                self._valid_sample(sample_name,'write')
                try:
                    lsamples = list(self.samples_device.Samples)
                    # retrieve sample position from its name
                    sample_position = lsamples[lsamples.index(sample_name)-1]
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
            elif sample_position is not None:
                self._valid_sample(sample_position,'write')
                try:
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return

            if sample_size_x is not None and sample_size_y is not None and sample_size_z is not None:
                sample_size=[sample_size_x,sample_size_y,sample_size_z]
                try:
                    self.samples_device.ActiveSampleSize = sample_size
                except Exception as exception:
                    self._tango_error(exception)
                    return
        else:
            validator = Validator(self._valid_sample,'write')
            dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)

            ret = BlissDialog([[dlg_sample]],title='Write sample size').show()

            if ret != False:

                sample={}
                if ret[dlg_sample] in self.sample_positions:
                    sample['position'] = ret[dlg_sample]
                else:
                    try:
                        lsamples = list(self.samples_device.Samples)
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    # retrieve sample position from its name
                    sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]

                try:
                    # select a sample from its position
                    self.samples_device.ActivateSample(sample['position'])
                except Exception as exception:
                    self._tango_error(exception)
                    return

                dlg_sizex = UserFloatInput(label="Sample size in X (mm)", defval=0.0)
                dlg_sizey = UserFloatInput(label="Sample size in Y (mm)", defval=0.0)
                dlg_sizez = UserFloatInput(label="Sample size in Z (mm)", defval=0.0)

                ret = BlissDialog([[dlg_sizex],[dlg_sizey],[dlg_sizez]],title='Write sample size').show()

                if ret != False:

                    sample_size=[ret[dlg_sizex],ret[dlg_sizey],ret[dlg_sizez]]
                    try:
                        self.samples_device.ActiveSampleSize = sample_size
                    except Exception as exception:
                        self._tango_error(exception)
                        return

    #
    # Read all alignment axis positions of a sample and return the values if sample is aligned
    #
    def read_align_pos(self, sample_name=None, sample_position=None):

        if sample_name is not None or sample_position is not None:
            if sample_name is not None:
                self._valid_sample(sample_name,'read')
                try:

                    lsamples = list(self.samples_device.Samples)
                    # retrieve sample position from its name
                    sample_position = lsamples[lsamples.index(sample_name)-1]
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
            elif sample_position is not None:
                self._valid_sample(sample_position,'read')
                try:
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
            try:
                align = self.samples_device.ActiveAligned
                sample = {}
                sample['alignsx'] = self.samples_device.ActiveSampleAlignPos[0]
                sample['alignsy'] = self.samples_device.ActiveSampleAlignPos[1]
                sample['alignsz'] = self.samples_device.ActiveSampleAlignPos[2]
            except Exception as exception:
                self._tango_error(exception)
                return

            if align:
                print('\n')
                for dim in ['X','Y','Z']:
                    key = 'aligns'+dim.lower()
                    print(f"Alignment position in {dim} (mm): {sample[key]}")
                print('\n')
            else:
                print(f"The sample {sample_position} is not aligned yet!\n")

        else:
            validator = Validator(self._valid_sample,'read')
            dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)

            ret = BlissDialog([[dlg_sample]],title='Read sample alignment position').show()

            if ret != False:

                sample={}
                if ret[dlg_sample] in self.sample_positions:
                    sample['position'] = ret[dlg_sample]
                else:
                    try:
                        lsamples = list(self.samples_device.Samples)
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    # retrieve sample position from its name
                    sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]

                try:
                    # select a sample from its position
                    self.samples_device.ActivateSample(sample['position'])
                except Exception as exception:
                    self._tango_error(exception)
                    return

                try:
                    sample['alignsx'] = self.samples_device.ActiveSampleAlignPos[0]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['alignsy'] = self.samples_device.ActiveSampleAlignPos[1]
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    sample['alignsz'] = self.samples_device.ActiveSampleAlignPos[2]
                except Exception as exception:
                    self._tango_error(exception)
                    return

                try:
                    # check if sample is aligned
                    align = self.samples_device.ActiveAligned
                except Exception as exception:
                    self._tango_error(exception)
                    return
                if align:
                    print('\n')
                    for dim in ['X','Y','Z']:
                        key = 'aligns'+dim.lower()
                        print(f"Alignment position in {dim} (mm): {sample[key]}")
                    print('\n')
                else:
                    print(f"The sample {sample['position']} is not aligned yet!\n")

    #
    # Define manually sx, sy, sz positions when sample is aligned = sample alignment position
    #
    def write_align_pos(self, sample_name=None, sample_position=None, sample_align_x=None, sample_align_y=None, sample_align_z=None):

        if sample_name is not None or sample_position is not None:
            if sample_name is not None:
                self._valid_sample(sample_name,'write')
                try:
                    lsamples = list(self.samples_device.Samples)
                    # retrieve sample position from its name
                    sample_position = lsamples[lsamples.index(sample_name)-1]
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return
            elif sample_position is not None:
                self._valid_sample(sample_position,'write')
                try:
                    self.samples_device.ActivateSample(sample_position)
                except Exception as exception:
                    self._tango_error(exception)
                    return

            if sample_align_x is not None and sample_align_y is not None and sample_align_z is not None:
                align_pos=[sample_align_x,sample_align_y,sample_align_z]
                try:
                    self.samples_device.ActiveSampleAlignPos = align_pos
                    # sample is now aligned
                    self.samples_device.ActiveAligned = True
                except Exception as exception:
                    self._tango_error(exception)
                    return
        else:

            validator = Validator(self._valid_sample,'write')
            dlg_sample = UserInput(label="Sample position or name", defval="A1 or Name", validator=validator)

            ret = BlissDialog([[dlg_sample]],title='Write sample alignment position').show()

            if ret != False:

                sample={}
                if ret[dlg_sample] in self.sample_positions:
                    sample['position'] = ret[dlg_sample]
                else:
                    try:
                        lsamples = list(self.samples_device.Samples)
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    # retrieve sample position from its name
                    sample['position'] = lsamples[lsamples.index(ret[dlg_sample])-1]

                try:
                    # select a sample from its position
                    self.samples_device.ActivateSample(sample['position'])
                except Exception as exception:
                    self._tango_error(exception)
                    return

                dlg_alignsx = UserFloatInput(label="Alignment position in X (mm)", defval=0.0)
                dlg_alignsy = UserFloatInput(label="Alignment position in Y (mm)", defval=0.0)
                dlg_alignsz = UserFloatInput(label="Alignment position in Z (mm)", defval=0.0)

                ret = BlissDialog([[dlg_alignsx],[dlg_alignsy],[dlg_alignsz]],title='Write sample alignment position').show()

                if ret != False:

                    align_pos = [ret[dlg_alignsx],ret[dlg_alignsy],ret[dlg_alignsz]]
                    try:
                        self.samples_device.ActiveSampleAlignPos = align_pos
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    try:
                        # sample is now aligned
                        self.samples_device.ActiveAligned = True
                    except Exception as exception:
                        self._tango_error(exception)
                        return

    #
    # Define actual sx, sy, sz positions as sample alignment position
    #
    def store_sample_align_pos(self):

        # get the position of the loaded sample
        try:
            sample_pos = self.robot_device.SamplePosition
        except Exception as exception:
            self._tango_error(exception)
            return

        if sample_pos != "None":
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(sample_pos)
            except Exception as exception:
                self._tango_error(exception)
                return

            try:
                sample_name = self.samples_device.ActiveSampleName
            except Exception as exception:
                self._tango_error(exception)
                return

            dlg_message = UserMsg(label=f"Sample {sample_name} is now aligned")
            dlg_confirm = UserMsg(label=f"Sample alignment positions are going to be stored. Continue?")
            ret = BlissDialog([[dlg_message],[dlg_confirm]],title='Store sample alignment positions').show()

            if ret != False:
                align_pos = [self.axis.get('sx').position,self.axis.get('sy').position,self.axis.get('sz').position]
                try:
                    self.samples_device.ActiveSampleAlignPos = align_pos
                except Exception as exception:
                    self._tango_error(exception)
                    return
                try:
                    # sample is now aligned
                    self.samples_device.ActiveAligned = True
                except Exception as exception:
                    self._tango_error(exception)
                    return

                print(f"\nStored alignment positions: {align_pos[0]}, {align_pos[1]}, {align_pos[2]}\n")
        else:
            print("No sample loaded by the sample changer!")

    def store_experiment_positions(self):
        
        if f"{current_session.name.split('TOMO')[0]}" == 'MR':
            index = 1
        else:
            index = 0
        self.experiment_positions['xc'] = self.axis.get('xc').position
        self._config['positions'][index][f"{current_session.name.split('TOMO')[0]}"]['experiment']['xc'] = self.experiment_positions['xc']

        self.experiment_positions['yrot'] = self.axis.get('yrot').position
        self._config['positions'][index][f"{current_session.name.split('TOMO')[0]}"]['experiment']['xc'] = self.experiment_positions['xc']

        self.experiment_positions['sz'] = self.axis.get('sz').position
        self._config['positions'][index][f"{current_session.name.split('TOMO')[0]}"]['experiment']['xc'] = self.experiment_positions['xc']

        string = f"\nStored experiment positions: xc: {self.experiment_positions['xc']}, yrot: {self.experiment_positions['yrot']}"

        if 'HRTOMO' in current_session.name:
            self.experiment_positions['z0'] = self.axis.get('z0').position
            self._config['positions'][index][f"{current_session.name.split('TOMO')[0]}"]['experiment']['z0'] = self.experiment_positions['z0']
            string += f", z0: {self.experiment_positions['z0']}"

        
        self._config.save()

        print(string+"\n")
    #
    # Show all defined samples
    #
    def show_samples(self,wait=True):

        try:
            samples = self.samples_device.Samples
        except Exception as exception:
            self._tango_error(exception)
            return
        sample = {}

        if samples is None:
            raise RuntimeError("No sample is defined")
        # define table columns
        table = []
        header=["Position","Name","Size X","Size Y","Size Z","Align SX","Align SY","Align SZ"]
        table.append(header)

        for i in range(0,len(samples),2):
            try:
                # select a sample from its position
                self.samples_device.ActivateSample(samples[i])
            except Exception as exception:
                self._tango_error(exception)
                return
            sample['position'] = samples[i]
            try:
                sample['name'] = self.samples_device.ActiveSampleName
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['size'] = self.samples_device.ActiveSampleSize
            except Exception as exception:
                self._tango_error(exception)
                return
            try:
                sample['alignpos'] = self.samples_device.ActiveSampleAlignPos
            except Exception as exception:
                self._tango_error(exception)
                return
            # add each sample in table
            sample_to_add = [str(sample['position']),str(sample['name']),str(sample['size'][0]),str(sample['size'][1]),str(sample['size'][2]),str(sample['alignpos'][0]),str(sample['alignpos'][1]),str(sample['alignpos'][2])]
            table.append(sample_to_add)

        print(tabulate(table, headers='firstrow', tablefmt='fancy_grid'))
    #
    # Delete all available samples
    #
    def reset_samples(self):

        dlg_confirm = UserMsg(label=f"All defined samples are going to be deleted. Continue?")
        ret = BlissDialog([[dlg_confirm]],title='Reset samples').show()

        if ret != False:
            try:
                self.samples_device.Reset()
            except Exception as exception:
                self._tango_error(exception)
                return
            print("All defined samples are deleted!")


    #
    # Sample changer robot calls
    #
    def load_sample(self, sample):

        try:
            try:
                lsamples = list(self.samples_device.Samples)
            except Exception as exception:
                self._tango_error(exception)
                return
            # test that sample exits and is added
            if sample in lsamples:
                if sample not in self.sample_positions :
                    # get sample position by sample name
                    sample = lsamples[lsamples.index(sample)-1]

                try:
                    self.samples_device.ActivateSample(sample)
                except Exception as exception:
                    self._tango_error(exception)
                    return

                # read alignment positions when aligned
                try:
                    align = self.samples_device.ActiveAligned
                except Exception as exception:
                    self._tango_error(exception)
                    return

                if align:
                    try:
                        align_pos = self.samples_device.ActiveSampleAlignPos
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                else:
                    # if not aligned, go to the default experiment positions
                    align_pos = [0,0,self.experiment_positions['sz']]

                print("\nMove to sample change position")
                self.change_position()
                try:
                    self.robot_device.LoadSample(sample)
                except Exception as exception:
                    self._tango_error(exception)
                    return

                print(f"\nLoading sample {sample}")
                print("Robot moving...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                while robot_state == 6:
                    time.sleep(3)
                    print("Robot moving...",end="\r")
                    try:
                        robot_state = self.robot_device.State()
                    except Exception as exception:
                        self._tango_error(exception)
                        return

                time.sleep(1)
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                if robot_state == 0:
                    print("\nMove to experiment position")
                    self.experiment_position(align_pos[0],align_pos[1],align_pos[2])
                    print("\nSample loading done!")

                    if self.robot_device.RemoteMode:
                        print("Beam shutter opening")
                        self.shutter.open()
                else:
                    print("\nSample loading did not finish as expected!")
                    try:
                        robot_status = self.robot_device.Status()
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    print(f"\nThe sample changer status is:{robot_status}")
            else:
                print('This sample does not exist')

        except KeyboardInterrupt:
            print('\n\nSample loading aborted')
            self.abort()
            raise

    def unload_sample(self):

        try:

            if self.robot_device.RemoteMode:
                print("Beam shutter closing")
                self.shutter.close()

            print("\nMove to sample change position")
            self.change_position()

            # unload activated sample loaded on tomograph
            sample = self.samples_device.ActiveSamplePosition
            print(f"\nUnloading sample {sample}")
            print("Robot moving...",end="\r")
            try:
                self.robot_device.UnloadSample()
            except Exception as exception:
                self._tango_error(exception)
                return

            try:
                robot_state = self.robot_device.State()
            except Exception as exception:
                self._tango_error(exception)
                return
            while robot_state == 6:
                time.sleep(3)
                print("Robot moving...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return

            time.sleep(1)
            try:
                robot_state = self.robot_device.State()
            except Exception as exception:
                self._tango_error(exception)
                return
            if robot_state == 0:
                print("\nSample unloading done!")
            else:
                print("\nSample unloading did not finish as expected!")
                try:
                    robot_status = self.robot_device.Status()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                print(f"\nThe sample changer status is:{robot_status}")

        except KeyboardInterrupt:
            print('\n\nSample unloading aborted')
            self.abort()
            raise

    def align(self):
        try:
            print("Move to alignment position")
            self.change_position()

            print("\nYou need enter the hutch and survey the procedure with the local control!")
            print("For the alignment the sample changer has to be in manual mode!")

            confirm = UserYesNo(label="\nIs the sample changer in manual mode?",defval=True)
            ret = display(confirm, title='Align sample changer')

            if ret != False:
                print("\nStarting alignment procedure\n")
                try:
                    self.robot_device.Align()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                time.sleep(2)
                print("Robot is ready to be moved...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                # test to 6 == robot is moving, if not return error!
                while robot_state == 6:
                    time.sleep(3)
                    print("Robot is ready to be moved...",end="\r")
                    try:
                        robot_state = self.robot_device.State()
                    except Exception as exception:
                        self._tango_error(exception)
                        return

                time.sleep(1)
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                if robot_state == 0:
                    try:
                        is_aligned = self.robot_device.Aligned
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    if is_aligned == 1:
                        print("\n\nThe sample changer is aligned!")
                    else:
                        print("\n\nThe alignment procedure ended without error, but the alignment failed!")
                        print("Please check the sample changer!")

                else:
                    print("\nAlignment did not finish as expected!")
                    try:
                        robot_status = self.robot_device.Status()
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    print(f"\nThe sample changer status is:{robot_status}")
        except KeyboardInterrupt:
            print('\n\nRobot align aborted')
            self.abort()
            raise


    def abort(self):

        print("\nStopping all robot movements!\n")
        try:
            self.robot_device.Abort()
        except Exception as exception:
            self._tango_error(exception)
            return

    def parking_position(self):

        print("\nYou need enter the hutch and survey the procedure with the local control!")
        print("For the recovery procedure, the sample changer has to be in manual mode!")

        confirm = UserYesNo(label="\nIs the sample changer in manual mode",defval=True)
        ret = display(confirm, title='Parking position')

        if ret != False:
            print("\nStarting recovery procedure\n")
            try:
                self.robot_device.ForceParkingPosition()
            except Exception as exception:
                self._tango_error(exception)
                return

            time.sleep(2)
            print("Robot moving...",end="\r")
            try:
                robot_state = self.robot_device.State()
            except Exception as exception:
                self._tango_error(exception)
                return
            # test to 6 == robot is moving, if not return error!
            while robot_state == 6:
                time.sleep(3)
                print("Robot moving...",end="\r")
                try:
                    robot_state = self.robot_device.State()
                except Exception as exception:
                    self._tango_error(exception)
                    return

            time.sleep(1)
            try:
                robot_state = self.robot_device.State()
            except Exception as exception:
                self._tango_error(exception)
                return
            if robot_state == 8:
                try:
                    is_parking = self.robot_device.ParkingPosition
                except Exception as exception:
                    self._tango_error(exception)
                    return
                if is_parking == 1:
                    print("\n\nReset the fault state")
                    try:
                        self.robot_device.Reset()
                    except Exception as exception:
                        self._tango_error(exception)
                        return
                    self.manual_unload()
                else:
                    print("\n\nThe recovery procedure ended without error, but the parking position was not reached")
                    print("Please check the sample changer!")
            else:
                print("\nRecover did not finish as expected!")
                try:
                    robot_status = self.robot_device.Status()
                except Exception as exception:
                    self._tango_error(exception)
                    return
                print(f"\nThe sample changer status is:{robot_status}")


    #
    # unset sample loaded
    #
    def manual_unload(self):

        confirm = UserYesNo(label="Please remove the sample manually\n Is the sample removed now?",defval=True)
        ret = display(confirm, title='Manual unload')

        if ret != False:
            try:
                self.robot_device.ManualUnload()
            except Exception as exception:
                self._tango_error(exception)
                return

    def open_gripper(self):

        print("\nOpen the gripper\n")
        try:
            self.robot_device.OpenGripper()
        except Exception as exception:
            self._tango_error(exception)
            return

    def close_gripper(self):

        print("\nClose the gripper\n")
        try:
            self.robot_device.CloseGripper()
        except Exception as exception:
            self._tango_error(exception)
            return

    @property
    def speed(self):
        return self.robot_device.speed

    @speed.setter
    def speed(self, value):
        self.robot_device.speed = value
        
    #
    # Robot positioning
    #

    def change_position(self):

        # Move the detector away
        det_pos = self.axis.get('xc').position
        print(f"Current detector position: {det_pos}")
        if self.robot_positions['xc'] > det_pos:
            det_pos = self.robot_positions['xc']
            print("\nMove the detector away")
            print(f"Move detector to: {det_pos}")
            umv(self.axis.get('xc'),det_pos)

        # umvd equivalent, must be change when bliss will be updated
        dial_pos1 = self.robot_positions['yrot']
        posyrot = self.axis.get('yrot').sign*dial_pos1 + self.axis.get('yrot').offset

        if 'z0' in self.axis:
            pos = self.robot_positions['z0']
            print(f"\nMove z0 to: {pos}")
            umv(self.axis.get('z0'),pos)

        # move the dial positions
        # umvd equivalent, must be change when bliss will be updated
        dial_pos1 = self.robot_positions['srot']
        pos1 = self.axis.get('srot').sign*dial_pos1 + self.axis.get('srot').offset
        print(f"\nMove srot to {pos1}")
        dial_pos2 = self.robot_positions['sx']
        pos2 = self.axis.get('sx').sign*dial_pos2 + self.axis.get('sx').offset
        print(f"\nMove sx to {pos2}")
        dial_pos3 = self.robot_positions['sy']
        pos3 = self.axis.get('sy').sign*dial_pos3 + self.axis.get('sy').offset
        print(f"\nMove sy to {pos3}")
        dial_pos4 = self.robot_positions['sz']
        pos4 = self.axis.get('sz').sign*dial_pos4 + self.axis.get('sz').offset
        print(f"\nMove sz to {pos4}")
        umv(self.axis.get('srot'),pos1,self.axis.get('sx'),pos2,self.axis.get('sy'),pos3,self.axis.get('sz'),pos4,self.axis.get('yrot'),posyrot)


    def experiment_position(self,sx_pos,sy_pos,sz_pos):

        print(f"\nMove sz to: {sz_pos}")
        umv(self.axis.get('sx'),sx_pos,self.axis.get('sy'),sy_pos,self.axis.get('sz'),sz_pos)

        if 'z0' in self.axis:
            pos = self.experiment_positions['z0']
            print(f"\nMove z0 to: {pos}")
            umv(self.axis.get('z0'),pos)

        pos = self.experiment_positions['yrot']
        print(f"\nMove yrot to: {pos}")
        umv(self.axis.get('yrot'),pos)

        pos = self.experiment_positions['xc']
        print("\nMove the detector closer")
        print(f"Move detector to: {pos}")
        umv(self.axis.get('xc'),pos)


    def _tango_error(self,exception):
        print("Failed to execute: %s\n" % exception.args[0].origin)
        print(exception.args[0].desc)

    #
    # verify if a given position is valid (exists) or is not already used
    #
    def _valid_sample(self,str_input,action):
        try:
            lsamples = self.samples_device.Samples
            if lsamples is not None:
                lsamples = list(lsamples)
            else:
                lsamples = list()
        except Exception as exception:
            self._tango_error(exception)
            return
        if action == 'add':
            if str_input in lsamples:
                raise ValueError(f"A sample is already defined at position {str_input}")
            elif str_input not in self.sample_positions:
                raise ValueError("This position does not exist, Please enter a new one")
        if action != 'add' and str_input not in lsamples:
            raise ValueError(f"This sample does not exist, Please enter a new one")


# load and unload all defined samples
def load_unload_samples():

    sample_changer = get_config().get('samplechanger')

    lsamples = sample_changer.samples_device.Samples

    for i in range(0,len(lsamples),2):
        if lsamples[i] != 'A18':
            print("-------------------------------")
            print(datetime.datetime.now())
            print("-------------------------------")
            print(f'Change number {lsamples[i][1:]}\n')
            print(f"Bench sample_changer.load_sample('{lsamples[i]}')\n")
            with bench():
                sample_changer.load_sample(lsamples[i])
            time.sleep(5)
            with bench():
                sample_changer.unload_sample()
            time.sleep(5)

    print("J ai fini")


# return robot status (ON,FAULT,DISABLE,MOVING,UNKNOWN)
def robot_status():

    sample_changer = get_config().get('samplechanger')

    print(sample_changer.robot_device.Status())
